'''
Created on Aug 15, 2020

@author: bo
'''

from skimage.measure import label, regionprops, regionprops_table
from skimage.transform import  resize
import config
import os
import pickle
import json    
import gzip
import numpy as np 

import tensorflow as tf 

from tensorflow.keras import layers
from model.impala_cnn_tf import make_model
from tensorflow.keras.utils import multi_gpu_model
import sys


def get_crop(label_img, prop):
    minr, minc, maxr, maxc = prop.bbox
    r = np.where(label_img == prop.label, 1, 0)
    return r[minr:maxr + 1, minc:maxc + 1].astype(np.int8)

    
def extract_mask(img, threshold=0.03):
    label_img = label(img)
    regions = regionprops(label_img)
    img_area = img.shape[0] * img.shape[1]
    ret = []
    for prop in regions:
        
        if prop.area > img_area * threshold:
            ret.append([1, get_crop(label_img, prop)])
        else:
            ret.append([0, get_crop(label_img, prop)])
    return ret


def central_img(img, size=(224, 224), final_size=None):
    size = tuple(size)
    final_size = tuple(final_size)
    m, n = img.shape
    h, w = size
    h = max(h, m)
    w = max(n, w)
    if m < h:
        newimg = np.zeros([h, n])
        newimg[(h - m) // 2:(h - m) // 2 + m, :] = img
        img = newimg
    if n < w:
        newimg = np.zeros([h, w])
        newimg[:, (w - n) // 2:(w - n) // 2 + n] = img
        img = newimg
    m, n = img.shape
    h, w = size
    if m > h or n > w:
        ret = resize(img, size, preserve_range=True)
    else:
        ret = img
        
    if(final_size is not None):
        ret = resize(ret, final_size, preserve_range=True)
        assert ret.shape == final_size
    else:
        assert ret.shape == size
    return ret
        

def _load_raw_data():
    file = os.path.join(config.OSIC_INPUT_HOME, "phase1_mask_training.pkl")

    with gzip.open(os.path.join(config.OSIC_HOME, "info", "phase1_mask_postives.json.gz")) as f:
        pos = set(json.load(f))
        print("loadded {} positive labels".format(len(pos)))    
    with gzip.open(file, 'r') as f:
        ret2 = pickle.load(f)
        print("loadded {} records".format(len(ret2)))

    ret = []
    for i, (_, x) in enumerate(ret2):
        label = 1 if i in pos else 0 
        ret.append([label, x])
    np.random.shuffle(ret)    
    return ret


def create_dataset(valid_ratio=0.2):
    ret = _load_raw_data()
    k = int(len(ret) * valid_ratio)
    valid = ret[:k]
    train = ret[k:]
        
    return train, valid 


def _float_feature(value):
    """Returns a float_list from a float / double."""
    return tf.train.Feature(float_list=tf.train.FloatList(value=[value]))


def _int64_feature(value):
    """Returns an int64_list from a bool / enum / int / uint."""
    return tf.train.Feature(int64_list=tf.train.Int64List(value=[value]))


def _bytes_feature(value):
  return tf.train.Feature(bytes_list=tf.train.BytesList(value=[value]))


def create_input_data(input_folder, imgsize):
    options = tf.io.TFRecordOptions(compression_type='GZIP')
    data = _load_raw_data()
    batch_size = len(data) // 128
    n_files = int(np.ceil(len(data) / batch_size))
    k = 0
    for i in range(n_files):
    # for i in range(3):
        filename = os.path.join(input_folder, "part{}.tfrecord".format(i))
        print("Writing " + filename)
        with tf.io.TFRecordWriter(filename, options=options) as writer: 
            for _ in range(batch_size):
                if k < len(data):
                    label, img = data[k]
                    img = (central_img(img * 255, PHASE1_IMGSIZE_BEFORE_RESHAPE, final_size=imgsize)) / 255.0;
                    img = np.expand_dims(img, -1).astype(np.float32)
                    feature = {
                        'height': _int64_feature(img.shape[0]),
                        'width': _int64_feature(img.shape[1]),
                        'channel': _int64_feature(1),
                        'label': _int64_feature(label),
                        'img_raw': _bytes_feature(img.reshape(-1).tobytes()),
                    }    
                    example = tf.train.Example(features=tf.train.Features(feature=feature))
                    writer.write(example.SerializeToString())
                    k += 1


image_feature_description = {
    'label': tf.io.FixedLenFeature([], tf.int64),
    'height': tf.io.FixedLenFeature([], tf.int64),
    'width': tf.io.FixedLenFeature([], tf.int64),
    'channel': tf.io.FixedLenFeature([], tf.int64),
    'img_raw': tf.io.FixedLenFeature([], tf.string),
}


def _test_read_data(datafolder):
    import matplotlib.pyplot as plt
    filenames = [os.path.join(datafolder, u) for u in os.listdir(datafolder)]
    raw_dataset = tf.data.TFRecordDataset(filenames=filenames, compression_type='GZIP')

    def _parse_image_function(example_proto):
        # Parse the input tf.Example proto using the dictionary above.
        return tf.io.parse_single_example(example_proto, image_feature_description)

    parsed_image_dataset = raw_dataset.map(_parse_image_function)
    
    plt.figure(figsize=(10, 10))
    for i, image_features in enumerate(parsed_image_dataset.take(9)):
        _ax = plt.subplot(3, 3, i + 1)
        img_raw = image_features['img_raw'].numpy()
        shape = (image_features['height'].numpy().astype("int")
            , image_features['width'].numpy().astype("int"),
            image_features['channel'].numpy().astype("int"))            
        img = np.frombuffer(img_raw, dtype=np.float32).reshape(shape)
        print(img.min(), img.max())
        plt.imshow(img)
        plt.title(str(image_features['label'].numpy().astype("uint8")))
        plt.axis("off")
    plt.show()

        
class DataGenerator(object):

    def __init__(self, data, batch_size, imgsize):
        self.data = data        
        np.random.shuffle(self.data)
        self.batch_size = batch_size
        self.imgsize = imgsize
        self.n = 0

    def __iter__(self):
        return self

    # Python 3 compatibility
    def __next__(self):
        return self.next()

    def next(self):
        while True:
            X = []
            labels = []
            while len(X) < self.batch_size:
                if self.n >= len(self.data):
                    self.n = 0
                    np.random.shuffle(self.data)
                label, x = self.data[self.n]
                labels.append(label)
                x = (central_img(x * 255, PHASE1_IMGSIZE_BEFORE_RESHAPE, final_size=self.imgsize)).astype(np.float32) / 255.0;
                x = np.expand_dims(x, -1)
                X.append(x);
                self.n += 1;
            X = np.array(X, dtype=np.float32)
            if False:
                b = np.zeros((labels.size, 2))
                b[np.arange(labels.size), label] = 1
                labels = np.array(b, dtype=np.float32)
            else:
                labels = np.array(labels, dtype=np.float32)
            yield (X, labels)

        
def train_model_with_memory(batch_size=128, imgsize=(128, 128)):
    train, valid = create_dataset()
    train_generator = DataGenerator(train, batch_size=batch_size, imgsize=imgsize)
    valid_generator = DataGenerator(valid, batch_size=batch_size, imgsize=imgsize)
    output_types = (tf.float32, tf.float32)
    output_shapes = (tf.TensorShape((batch_size, imgsize[0], imgsize[1], 1)), tf.TensorShape((batch_size,)))
    train_ds = tf.data.Dataset.from_generator(lambda: train_generator.next() , output_types=output_types,
                                            output_shapes=output_shapes)
    val_ds = tf.data.Dataset.from_generator(lambda:   valid_generator.next(), output_types=output_types,
                                            output_shapes=output_shapes)
    
    steps_per_epoch = int(210979 * 0.8 / batch_size)
    validation_steps = int(210979 * 0.2 / batch_size)
    input_shape = (*imgsize, 1)
    if False:
        import matplotlib.pyplot as plt
        plt.figure(figsize=(10, 10))
        for images, labels in train_ds.take(1):
            for i in range(9):
                _ax = plt.subplot(3, 3, i + 1)
                print(images[i].numpy().min(), images[i].numpy().max())
                plt.imshow(images[i].numpy())
                plt.title(str(labels[i].numpy().astype("uint8")))
                plt.axis("off")
        plt.show()
    _train_model(input_shape, train_ds, val_ds, steps_per_epoch, validation_steps)


def train_model_with_files(batch_size=128, imgsize=(128, 128)):
    filenames = [os.path.join(datafolder, u) for u in os.listdir(datafolder)]
    dataset = tf.data.TFRecordDataset(filenames=filenames, compression_type='GZIP')

    def _parse_image_function(example_proto):
        # Parse the input tf.Example proto using the dictionary above.
        return tf.io.parse_single_example(example_proto, image_feature_description)

    dataset = dataset.map(_parse_image_function)
    
    def _to_image_and_label(image_features):
        img_raw = tf.io.decode_raw(image_features['img_raw'], out_type=tf.float32)
        img_shape = tf.stack([image_features['height'], image_features['width'], image_features['channel']])
        img = tf.reshape(img_raw, img_shape)
        return img, image_features['label']
    
    dataset = dataset.map(_to_image_and_label)

    def is_test(x, y):
        return x % 5 == 0
    
    def is_train(x, y):
        # return not is_test(x, y)
        return True
    
    recover = lambda _x, y: y
    
    val_ds = dataset.enumerate() \
                        .filter(is_test) \
                        .map(recover).batch(batch_size)
    
    train_ds = dataset.enumerate() \
                        .filter(is_train) \
                        .map(recover).batch(batch_size).shuffle(100)   
    input_shape = (*imgsize, 1)
    
    if False:
        import matplotlib.pyplot as plt
        plt.figure(figsize=(10, 10))
        for images, labels in train_ds.take(1):
            for i in range(9):
                _ax = plt.subplot(3, 3, i + 1)
                print(images[i].numpy().min(), images[i].numpy().max())
                plt.imshow((images[i].numpy() * 255).astype("uint8"))
                plt.title(str(labels[i].numpy().astype("uint8")))
                plt.axis("off")
        plt.show()    
    _train_model(input_shape, train_ds, val_ds, None, None)


def _train_model(input_shape, train_ds, val_ds, steps_per_epoch, validation_steps):
    model = make_model(input_shape=input_shape)
    # (model.summary())

    try:
        model = multi_gpu_model(model, cpu_merge=False)
        print("Training using multiple GPUs..")
    except:
        print("Training using single GPU or CPU..")
         
    model.compile(optimizer='adam',
              loss=tf.keras.losses.SparseCategoricalCrossentropy(from_logits=True),
              metrics=['accuracy'])

    AUTOTUNE = tf.data.experimental.AUTOTUNE
    train_ds = train_ds.prefetch(buffer_size=AUTOTUNE)
    val_ds = val_ds.prefetch(buffer_size=AUTOTUNE)

    tensorboard_callback = tf.keras.callbacks.TensorBoard(log_dir="./logs")
    earlystopping_callback = tf.keras.callbacks.EarlyStopping(monitor='loss', patience=3)
    model_checkpoint_callback = tf.keras.callbacks.ModelCheckpoint(
        filepath="./phase1_mask/phase1_mask",
        save_weights_only=True,
        monitor='val_accuracy',
        mode='max',
        save_best_only=True)

    epochs = 20
    history = model.fit(
      train_ds,
      validation_data=val_ds,
      epochs=epochs,
      steps_per_epoch=steps_per_epoch,
      validation_steps=validation_steps,
      callbacks=[tensorboard_callback, earlystopping_callback, model_checkpoint_callback]
      
    )
    
    if False:
        import matplotlib.pyplot as plt
        print(list(history.history.keys()))
        acc = history.history['accuracy']
        val_acc = history.history['val_accuracy']
        
        loss = history.history['loss']
        val_loss = history.history['val_loss']
        
        epochs_range = range(epochs)
        
        plt.figure(figsize=(8, 8))
        plt.subplot(1, 2, 1)
        plt.plot(epochs_range, acc, label='Training Accuracy')
        plt.plot(epochs_range, val_acc, label='Validation Accuracy')
        plt.legend(loc='lower right')
        plt.title('Training and Validation Accuracy')
        
        plt.subplot(1, 2, 2)
        plt.plot(epochs_range, loss, label='Training Loss')
        plt.plot(epochs_range, val_loss, label='Validation Loss')
        plt.legend(loc='upper right')
        plt.title('Training and Validation Loss')
        plt.show()


PHASE1_IMGSIZE = (128, 128)        
PHASE1_IMGSIZE_BEFORE_RESHAPE = (256, 256)
    
if __name__ == '__main__':
    batch_size = 128
    imgsize = PHASE1_IMGSIZE

    datafolder = os.path.join(config.OSIC_INPUT_HOME, "phase1_mask_train")    
    if len(sys.argv) == 2 and sys.argv[1] == 'create_data':
        config.create_folder_if_not_exists(datafolder)
        print("creating datafiles in " + datafolder)
        create_input_data(datafolder, imgsize=imgsize)
    elif len(sys.argv) == 2 and sys.argv[1] == 'test_data':
        _test_read_data(datafolder)        
    elif len(sys.argv) == 3 and sys.argv[1] == 'train':
        if sys.argv[2] == 'memory':
            print ("training with all data in memory and generated on the fly. may be slow")
            train_model_with_memory(batch_size=batch_size, imgsize=imgsize)
        if sys.argv[2] == 'file':
            print ("training with data in file system.")
            if not os.path.exists(datafolder):
                print("data not found, use create_data first")
            else:
                train_model_with_files(batch_size=batch_size, imgsize=imgsize)
    else:
        print("""
        usage: 
            python {} train <memory|file>
            or
            python {} create_data
        """.format(sys.argv[0], sys.argv[0]))
