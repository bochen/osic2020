'''
Created on Aug 28, 2020

@author: bo
'''

import os
from functools import partial

import numpy as np
import pandas as pd
import random
import math

from tqdm.notebook import tqdm

from sklearn.model_selection import StratifiedKFold, GroupKFold, KFold
from sklearn.metrics import mean_squared_error

from sklearn.linear_model import Ridge

import os
import numpy as np
import pandas as pd

from sklearn.model_selection import KFold
from sklearn.preprocessing import MinMaxScaler
from sklearn.metrics import mean_absolute_error

import robust_loss.general
import robust_loss.adaptive
import nevergrad as ng

import config 
import warnings
warnings.filterwarnings("ignore")

trainpath = '../input/train.csv'
testpath = '../input/test.csv'
submissionpath = '../input/sample_submission.csv'


def read_submission(path):
    submission_df = pd.read_csv(path)
    temp_sub_df = submission_df['Patient_Week'].str.split('_', expand=True)
    temp_sub_df.rename(columns={0: 'Patient', 1: 'Weeks'}, inplace=True)
    temp_sub_df['Weeks'] = temp_sub_df['Weeks'].astype(np.int)
    submission_df = pd.concat([submission_df, temp_sub_df], axis=1)
    submission_df = submission_df[['Patient', 'Weeks', 'Patient_Week']]
    return submission_df


def get_baseline(df):
    baseline = df[['Patient', 'Weeks', 'FVC', 'Percent']].sort_values(['Patient', 'Weeks']).groupby(['Patient']).first().reset_index()
    baseline.columns = ['Patient', 'base_Weeks', 'base_FVC', 'base_Percent']
    baseline['base_ref_FVC'] = baseline['base_FVC'].divide(baseline['base_Percent'] / 100)
    return baseline


def add_baseline(df):
    df = pd.merge(df, get_baseline(df), on='Patient', how='left')
    return df


def make_submission_df(submission_df, test_df):
    cols = set(test_df.columns).difference(submission_df.columns)
    cols = [u for u in cols if u not in ['FVC', 'Percent']]
    print(cols)
    df = pd.merge(submission_df, test_df[list(cols) + ["Patient"]], on='Patient', how='left')
    make_feature(df)
    return df


def make_feature(df):
    df['weeks_since_base'] = df['Weeks'] - df['base_Weeks']        

    
def preprocess(df):
    sex = {'Male': 0, 'Female': 1}
    smoke = {'Ex-smoker': 1, 'Never smoked': 0, 'Currently smokes': 2}
    df['Sex'] = df['Sex'].map(sex)
    df['SmokingStatus'] = df['SmokingStatus'].map(smoke)
    # df['SmokingStatus_0']=(df['SmokingStatus']==0).astype(np.int)
    df['SmokingStatus_1'] = (df['SmokingStatus'] == 1).astype(np.int)
    df['SmokingStatus_2'] = (df['SmokingStatus'] == 2).astype(np.int)
    df = df.drop(["SmokingStatus"], axis=1)
    return df


def expand_train(df):
    
    df = df.drop(['base_Weeks', 'base_FVC', 'base_Percent'], axis=1)
    dynamic_cols = ['Weeks', 'FVC', 'Percent'] 
    static_df = df.loc[:, ~df.columns.isin(dynamic_cols)].drop_duplicates()
    dynamic_df = df[["Patient"] + dynamic_cols].sort_values(['Patient', 'Weeks'])
    lst = []
    for patient in set(df['Patient'].values):
        subdf = dynamic_df[dynamic_df['Patient'] == patient]
        for i in range(subdf.shape[0]):
            lst.append(subdf.iloc[i].to_list() + subdf.iloc[i].to_list()[1:])
                    
        for i in range(subdf.shape[0]):
            for j in range(subdf.shape[0]):
                if i != j:
                    lst.append(subdf.iloc[j].to_list() + subdf.iloc[i].to_list()[1:])
    newdf = pd.DataFrame(lst, columns=['Patient', 'Weeks', 'FVC', 'Percent'] + ['base_Weeks', 'base_FVC', 'base_Percent'])
    
    newdf = pd.merge(newdf, static_df, how='left', on='Patient')
    return newdf
    

def read_data(path, is_train=False, expand=True):
    df = pd.read_csv(path)
    if is_train:
        df = df.groupby(['Patient', 'Weeks']).aggregate({'FVC':'mean', 'Percent':'mean', 'Age':'first', 'Sex':'first', 'SmokingStatus':'first'}).reset_index()
    else:
        assert df.shape[0] == df.groupby(['Patient'])['Age'].count().shape[0]
    df = preprocess(df) 
    df = add_baseline(df)
    if is_train and expand:
        df = expand_train(df)
        
    make_feature(df)
    return df


columns = ['FVC', 'Age', 'base_Percent', 'base_FVC', 'base_ref_FVC', 'weeks_since_base']
from sklearn.preprocessing import StandardScaler


def make_standard_scaler(rawtriandf, rawtestdf):
    df = pd.concat([rawtriandf, rawtestdf])
    d = {}
    for col in columns:
        scaler = StandardScaler().fit(df[col].values.reshape(-1, 1))
        d[col] = scaler
    return d


rawtraindf = read_data(trainpath, is_train=True, expand=False)
traindf = read_data(trainpath, is_train=True)
testdf = read_data(testpath)
scalers = make_standard_scaler(rawtraindf, testdf)
for col in columns:
    traindf['norm_' + col] = scalers[col].transform(traindf[col].values.reshape(-1, 1))
    if col not in ['FVC']:
        testdf['norm_' + col] = scalers[col].transform(testdf[col].values.reshape(-1, 1))

submissiondf = read_submission(submissionpath)
submissiondf = make_submission_df(submissiondf, testdf)
submissiondf['norm_' + 'weeks_since_base'] = scalers[col].transform(submissiondf['weeks_since_base'].values.reshape(-1, 1))

from sklearn.linear_model import Ridge


def remove_outlier(traindf):
    X_train = traindf.drop(['Patient', 'Weeks', 'FVC', 'Percent', 'base_Weeks', 'base_ref_FVC'], 1).values
    y_train = traindf['FVC'].values
    model = Ridge()
    model.fit(X_train, y_train)
    y_pred = model.predict(X_train)
    err = (y_train - y_pred)
    rmse = np.sqrt(np.mean((err) ** 2))
    print('rmse:', rmse, 'mean err:', np.mean(err))
    idx = np.where(np.abs(err) > 3 * rmse, 0, 1)
    return traindf.loc[idx > 0]


traindf = remove_outlier(traindf)

traindf = traindf[traindf['weeks_since_base'] > 0]
traindf.shape

FEATS = [x for x in traindf.columns if x.startswith('norm_') and x != "norm_FVC"] + ['Sex', 'SmokingStatus_1', 'SmokingStatus_2', ]

print(traindf[FEATS].describe())


def make_kfold(train_df, n_split=5):
    group_kfold = GroupKFold(n_splits=n_split)
    groups = train_df['Patient']
    folds = []
    for train_index, test_index in group_kfold.split(train_df['Patient'].values, groups=traindf['Patient'].values):
        folds.append((train_df.iloc[train_index], train_df.iloc[test_index]))
    return folds


folds = make_kfold(traindf)
len(folds)

print(scalers['FVC'].scale_, scalers['FVC'].mean_)

import tensorflow as tf
import tensorflow.keras.backend as K
import tensorflow.keras.layers as L
import tensorflow.keras.models as M

SCALE = tf.constant(scalers['FVC'].scale_[0], dtype='float32')
C1, C2 = tf.constant(70, dtype='float32') / SCALE, tf.constant(1000, dtype="float32") / SCALE


#=============================#
def laplace_loss(y_true, y_pred_and_sigma):
    fvc_pred = y_pred_and_sigma[:, 0]
    sigma = y_pred_and_sigma[:, 1]
    
    # sigma_clip = sigma + C1
    sigma_clip = tf.maximum(sigma, C1)
    delta = tf.abs(y_true[:, 0] - fvc_pred)
    delta = tf.minimum(delta, C2)
    sq2 = tf.sqrt(tf.dtypes.cast(2, dtype=tf.float32))
    metric = (delta / sigma_clip) * sq2 + tf.math.log(sigma_clip * sq2) + tf.math.log(SCALE)
    return K.mean(metric)


def sigma_loss(alf):
    
    def _sigma_loss(y_true, y_pred_and_sigma):
        fvc_pred = y_pred_and_sigma[:, 0][:, None]
        sigma_pred = y_pred_and_sigma[:, 1][:, None]

        expected_sigma = tf.abs(y_true - fvc_pred) * tf.sqrt(tf.constant(2, tf.float32))

        return tf.reduce_mean(alf((expected_sigma - sigma_pred)))

    return _sigma_loss


def fvc_loss(alf):

    def _fvc_loss(y_true, y_pred_and_sigma):
        fvc_pred = y_pred_and_sigma[:, 0][:, None]

        return tf.reduce_mean(alf((fvc_pred - y_true)))    

    return _fvc_loss

    
def loss(adaptive_lossfun_fvc, adaptive_lossfun_sigma):

    def _loss(y_true, y_pred_and_sigma):
        return fvc_loss(adaptive_lossfun_fvc)(y_true, y_pred_and_sigma) + sigma_loss(adaptive_lossfun_sigma)(y_true, y_pred_and_sigma)

    return _loss


#=================
def make_model(n_input, n_dense_layer=2, n_dense_size=128, clip_value=0.5, l1=0.01, l2=0.01):
    adaptive_lossfun_fvc = (
        robust_loss.adaptive.AdaptiveLossFunction(
            num_channels=1, float_dtype=np.float32))
    adaptive_lossfun_sigma = (
        robust_loss.adaptive.AdaptiveLossFunction(
            num_channels=1, float_dtype=np.float32))

    l12 = tf.keras.regularizers.l1_l2(l1=l1, l2=l2)
    z = L.Input((n_input,), name="feats_input")
    for i in range(n_dense_layer):
        x = L.Dense(n_dense_size, activation="linear", name="d" + str(i), kernel_regularizer=l12)(z)
    
    if 1:
        fvc_pred = L.Dense(1, activation="linear", name="fvc_pred_pre", kernel_regularizer=l12)(x)
        sigma = L.Dense(1, activation="linear", name="log_sigma_pred_pre", kernel_regularizer=l12)(x)
    
    preds = tf.stack([fvc_pred, sigma], axis=2)
    preds = tf.reshape(preds, (-1, 2))
    model = M.Model(z, preds, name="CNN")
    # optimizer = tf.keras.optimizers.Adam(learning_rate=0.01, beta_1=0.9, beta_2=0.999, epsilon=1e-08, clipvalue=0.5)
    optimizer = tf.keras.optimizers.RMSprop(learning_rate=0.001, clipvalue=clip_value)
    return model, optimizer, adaptive_lossfun_fvc, adaptive_lossfun_sigma
#     model.compile(loss=loss(adaptive_lossfun_fvc, adaptive_lossfun_sigma), optimizer=optimizer, 
#                   metrics=[laplace_loss, sigma_loss(adaptive_lossfun_sigma), fvc_loss(adaptive_lossfun_fvc)])
#     return model,adaptive_lossfun_fvc,adaptive_lossfun_sigma


def train(batch_size=256, num_epoch=100, n_dense_layer=2, n_dense_size=128, clip_value=0.5, l1=0.01, l2=0.01, verbose=False):
    BATCH_SIZE = batch_size
    SHUFFLE_BUFFER_SIZE = BATCH_SIZE * 3
    scores = []
    for a, b in folds[:]:
        model, optimizer, adaptive_lossfun_fvc, adaptive_lossfun_sigma = make_model(len(FEATS), n_dense_layer=n_dense_layer, n_dense_size=n_dense_size, clip_value=clip_value, l1=l1, l2=l2)

        variables = (
            list(model.trainable_variables) + 
            list(adaptive_lossfun_fvc.trainable_variables) + 
            list(adaptive_lossfun_sigma.trainable_variables))    

        train_X = a[FEATS].values.astype(np.float32)
        train_y = a['norm_FVC'].values.astype(np.float32)
        test_X = b[FEATS].values.astype(np.float32)
        test_y = b['norm_FVC'].values.astype(np.float32)

        train_dataset = tf.data.Dataset.from_tensor_slices((train_X, train_y))
        train_dataset = train_dataset.shuffle(SHUFFLE_BUFFER_SIZE).batch(BATCH_SIZE)
        
        best_score = np.inf
        
        for epoch in range(num_epoch):

            def lossfun(X, y):
                ys = model(X)
                u, v = fvc_loss(adaptive_lossfun_fvc)(y[:, None], ys), sigma_loss(adaptive_lossfun_sigma)(y[:, None], ys)
                return u + v, u, v

            def lossfun2(X, y):
                ys = model(X)
                u, v = fvc_loss(adaptive_lossfun_fvc)(y[:, None], ys), sigma_loss(adaptive_lossfun_sigma)(y[:, None], ys)
                w = laplace_loss(y[:, None], ys)
                return u + v, u, v, w

            for x, y in train_dataset:
                optimizer.minimize(lambda: lossfun(x, y)[0], variables)
            
            valid_loss, valid_loss_fvc, valid_loss_sigma, test_loss_laplace = lossfun2(test_X, test_y)                
            if best_score > test_loss_laplace.numpy():
                best_score = test_loss_laplace.numpy()
            if verbose and np.mod(epoch, 20) == 0:
                train_loss, train_loss_fvc, train_loss_sigma, train_loss_laplace = lossfun2(train_X, train_y)
                print(('{:<4}: tr_loss={:0.5f} tr_loss_fnv={:0.5f} tr_loss_sigma={:0.5f} tr_loss_laplace={:0.5f}' + 
                      ' test_loss={:0.5f} train_loss_fnv={:0.5f} train_loss_sigma={:0.5f} test_loss_laplace={:0.5f}').format(
                            epoch,
                            train_loss, train_loss_fvc, train_loss_sigma, train_loss_laplace,
                            valid_loss, valid_loss_fvc, valid_loss_sigma, test_loss_laplace,
                        ))            

                print('{:<4}: alpha_fvc={:0.5f}  scale_fvc={:0.5f} alpha_sigma={:0.5f}  scale_sigma={:0.5f}'.format(
                            epoch,
                            adaptive_lossfun_fvc.alpha()[0, 0],
                            adaptive_lossfun_fvc.scale()[0, 0],
                            adaptive_lossfun_sigma.alpha()[0, 0],
                            adaptive_lossfun_sigma.scale()[0, 0],
                        ))            
        
        scores.append(best_score)
    if verbose:
        print("scores: " + str(scores))
    return np.mean(scores)


def find_parameters():
    # Instrumentation class is used for functions with multiple inputs
    # (positional and/or keywords)
    parametrization = ng.p.Instrumentation(
        # learning_rate=ng.p.Log(lower=0.001, upper=1.0),
        # an integer from 1 to 12
        batch_size=ng.p.Scalar(lower=32, upper=1024).set_integer_casting(),
        n_dense_layer=ng.p.TransitionChoice([1, 2, 3, 4]),
        n_dense_size=ng.p.Scalar(lower=32, upper=256).set_integer_casting(),
        clip_value=ng.p.Scalar(lower=0.2, upper=5),
        l1=ng.p.Scalar(lower=0.0, upper=2),
        l2=ng.p.Scalar(lower=0, upper=2.0),
        # max_features=ng.p.Choice(["auto", "sqrt", 'log2'])
    )
    
    optimizer = ng.optimizers.TwoPointsDE(parametrization=parametrization, budget=1, num_workers=2)
    recommendation = optimizer.minimize(train)
    print(recommendation)
    print(recommendation.kwargs)
    return recommendation.kwargs


if __name__ == '__main__':
    find_parameters()
