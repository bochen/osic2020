'''
Created on Aug 13, 2020

@author: bo
'''

# Segmentation
import numpy as np 
import config
from phase1_mask_model import     central_img    
from lm_andrada import dicom_to_image, remove_board, get_users, \
    get_dcm_files, get_common_board

from tqdm import tqdm 
import os
from utils import pickle_dumpz
from lm_phase2 import _get_crop, softmax
import cv2 

OSIC_INPUT_HOME = config.OSIC_INPUT_HOME


def refine_input(im_th):
    cnts, hir = cv2.findContours(im_th, cv2.RETR_TREE, cv2.CHAIN_APPROX_NONE)
    areas = [cv2.contourArea(cnt) for cnt in cnts] 
    if len(areas) ==0 : return (im_th*0).astype(np.uint8)
    i = np.argsort(areas)[-1]
    cnt = cnts[i]
    a = np.zeros([*im_th.shape, 3], dtype=np.uint8)
    cv2.drawContours(a, cnts, i, (255, 255, 255), thickness=cv2.FILLED);
    mask = (a[:, :, 0] > 0).astype(np.uint8)
    kernel = cv2.getStructuringElement(cv2.MORPH_ELLIPSE, (3, 3))
    mask = cv2.dilate(mask, kernel, iterations=20)    
    return (mask > 0) * im_th

    
def refine_mask(mask):
    kernel = cv2.getStructuringElement(cv2.MORPH_ELLIPSE, (3, 3))
    mask = cv2.dilate(mask, kernel, iterations=20)

    cnts, hir = cv2.findContours(255 - mask * 255, cv2.RETR_TREE, cv2.CHAIN_APPROX_NONE)
    areas = [cv2.contourArea(cnt) for cnt in cnts] 
    if len(areas) ==0 : return (mask*0).astype(np.uint8)
    i = np.argsort(areas)[-1]
    cnt = cnts[i]
    a = np.zeros([*mask.shape, 3], dtype=np.uint8)
    cv2.drawContours(a, cnts, i, (255, 255, 255), thickness=cv2.FILLED);
    return a[:, :, 0]


def flushing(im_th, b_show=False):
    im_th = refine_input(im_th)
    im_floodfill = im_th.astype(np.uint8).copy() 
    h, w = im_th.shape[:2]
    mask = np.zeros((h + 2, w + 2), np.uint8)
    cv2.floodFill(im_floodfill, mask, (0, 0), 255);
    cv2.floodFill(im_floodfill, mask, (w - 1, 0), 255);
    cv2.floodFill(im_floodfill, mask, (0, h - 1), 255);
    cv2.floodFill(im_floodfill, mask, (w - 1, h - 1), 255);
    mask = refine_mask(mask)
    im_floodfill_inv = cv2.bitwise_not(im_floodfill)
    mask = mask[1:-1, 1:-1]
    im_out = mask * im_floodfill_inv
    if b_show:
        import matplotlib.pyplot as plt
        plt.subplot(2, 2, 1)
        plt.imshow(im_th, cmap='gray')  
        plt.axis('off')
        plt.title("orignal")
        plt.subplot(2, 2, 2)
        plt.axis('off')
        plt.imshow(mask, cmap='gray')    
        plt.title("mask")
        plt.subplot(2, 2, 3)
        plt.axis('off')
        plt.imshow(im_floodfill, cmap='gray')    
        plt.title("floodfill")
        plt.subplot(2, 2, 4)
        plt.axis('off')
        plt.imshow(im_out, cmap='gray')    
        plt.title("out")
        plt.show()
    return [im_out]


def preprocessing(img):
    a = img.copy()
    t = 700
    a[(a < t) * (a > -t)] = 0
    a[a != 0] = 255
    im_th = a.astype(np.uint8)
    return im_th


def phase4_make_lungmask(img, b_show=False):
    return flushing(preprocessing(img), b_show=b_show)


def pahse4_get_user_lung_masks(uid, is_train):
    files = get_dcm_files(uid, is_train)
    masks = []
    rawimages = []
    file_index = []
    for u in files:
        file_index.append(int(u.split("/")[-1].split('.')[0]))
        img = dicom_to_image(u, to_remove_board=False)
        rawimages.append(img)
    clip_positions = get_common_board(rawimages)
    for idx, rawimg in zip(file_index, rawimages):
        img = remove_board(rawimg, clip_positions)
        mask_list = phase4_make_lungmask(img)
        masks += [ (idx, u) for u in  mask_list]
    return masks


def run():
    outputdir = config.OSIC_SANDBOX_HOME + "/train_phase4_masks/"
    if not os.path.exists(outputdir): os.mkdir(outputdir)
    
    users = get_users(True)
    
    for uid in tqdm(users[:]):
        output2 = outputdir + uid + '.mask.gz'
        if os.path.exists(output2): continue
        masks = pahse4_get_user_lung_masks(uid, True)
        pickle_dumpz(masks, output2)
    

if __name__ == '__main__':
    if 0:
        import matplotlib.pyplot as plt
        # img = dicom_to_image("../input/train/ID00255637202267923028520/16.dcm", to_remove_board=True)
        # img = dicom_to_image("../input/train/ID00367637202296290303449/130.dcm", to_remove_board=True)
        # img = dicom_to_image("../input/train/ID00343637202287577133798/20.dcm", to_remove_board=True)
        # img = dicom_to_image("../input/train/ID00241637202264294508775/128.dcm", to_remove_board=True)
        # img = dicom_to_image("../input/train/ID00048637202185016727717/14.dcm", to_remove_board=True)
        # img = dicom_to_image("../input/train/ID00165637202237320314458/11.dcm", to_remove_board=True)
        img = dicom_to_image("../input/train/ID00381637202299644114027/31.dcm", to_remove_board=True)

        plt.imshow(img, cmap='gray')
        plt.show()
        phase4_make_lungmask(img, b_show=True)
    if 1:
        run() 
