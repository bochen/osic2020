import lm_andrada as lm
import config
import os 
from tqdm import tqdm

outputdir = config.OSIC_SANDBOX_HOME+"/train/"
if not os.path.exists(outputdir): os.mkdir(outputdir)

users=lm.get_users(True)

for uid in tqdm(users):
    output1 = outputdir + uid+'.img.gz'
    output2 = outputdir + uid+'.mask.gz'
    if os.path.exists(output1): continue
    print(uid)
    masks,img=lm.get_user_lungs(uid,True)
    lm.save_array_z(img,output1)
    lm.save_array_z(masks,output2)
