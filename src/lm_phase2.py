'''
Created on Aug 13, 2020

@author: bo
'''

# Segmentation
import numpy as np 
import config
from model.impala_cnn_tf import make_model
from phase1_mask_model import PHASE1_IMGSIZE, PHASE1_IMGSIZE_BEFORE_RESHAPE, \
    central_img
from lm_andrada import dicom_to_image, remove_board, make_lungmask, get_users, \
    get_dcm_files, get_common_board, save_array_z

from tqdm import tqdm 
import os
from utils import pickle_dumpz

OSIC_INPUT_HOME = config.OSIC_INPUT_HOME


def _get_crop(label_img, props, rawimg=None):
    if not isinstance(props, list):
        props = [props]
    minr, minc, maxr, maxc = props[0].bbox
    r = np.where(label_img == props[0].label, 1, 0)

    for prop in props[1:]:
        _minr, _minc, _maxr, _maxc = prop.bbox
        minr = min(minr, _minr)
        minc = min(minc, _minc)
        maxr = max(maxr, _maxr)
        maxc = max(maxc, _maxc)        
        r += np.where(label_img == prop.label, 1, 0)

    r = (r > 0).astype(np.int8)
    clipped_mask = r[minr:maxr + 1, minc:maxc + 1].astype(np.int8)
    if rawimg is not None:
        img = r * rawimg
        clipped_img = img[minr:maxr + 1, minc:maxc + 1]
        return clipped_mask, clipped_img
    else:
        return clipped_mask


def phase1_fun_to_get_good_labels(img, labeled_img, old_regions, b_show=False):
    assert img.shape == labeled_img.shape
    w, h = img.shape 
    fun0 = lambda B, row_size, col_size: B[2] - B[0] < row_size / 10 * 9 and B[3] - B[1] < col_size / 10 * 9 
    
    fun = lambda B, row_size, col_size: B[2] - B[0] < row_size / 10 * 9 and B[3] - B[1] < col_size / 10 * 9 \
                    and B[0] > row_size * 0.15 and B[2] < col_size * 0.85
    area = w * h
    good_masks0 = []
    good_regions = []
    
    regions = [x for x in old_regions if fun0(x.bbox, w, h)]
    max_region_area = np.max([x.area for x in regions])
    for prop in regions:
        # if prop.area / area > 0.03 or fun(prop.bbox, w,h):
        if prop.area / max_region_area > 0.10:
            if b_show:
                import matplotlib.pyplot as plt
                print(prop.area / area, prop.area / max_region_area)
                plt.imshow(_get_crop(labeled_img, prop));plt.show()
            
            good_masks0.append(_get_crop(labeled_img, prop))
            good_regions.append(prop)
    good_index = []
    if len(good_masks0) > 0:
        probs = phase2_predict(good_masks0)
        for i, p in enumerate(probs):
            if b_show:
                import matplotlib.pyplot as plt
                plt.imshow(good_masks0[i]);plt.title(str(p));plt.show()
            
            if p > 0.08:
                good_index.append(i)
    good_masks = []
    for i in good_index:
        good_masks.append(good_masks0[i])
    for i in good_index:
        for j in good_index:
            if(i < j):
                good_masks.append(_get_crop(labeled_img, [good_regions[i], good_regions[j]]))
                
    if b_show:
        import matplotlib.pyplot as plt
        for mask in good_masks:
            plt.imshow(mask)
            plt.show()
    return good_masks


def phase2_make_lungmask(img, b_show=False):
    return make_lungmask(img, fun_process_labels=lambda *args: phase1_fun_to_get_good_labels(*args, b_show=b_show))
 

_phase1_model = None 


def get_model():
    global _phase1_model
    if(_phase1_model is None):
        _phase1_model = make_model(input_shape=(*PHASE1_IMGSIZE, 1), num_outputs=2)
        _phase1_model.load_weights(os.path.join(config.OSIC_HOME, 'phase1_mask', 'phase1_mask'))
    return _phase1_model


def softmax(X):
    a = np.exp(X)
    return a / np.sum(a, axis=1, keepdims=True)


def phase2_predict(masks):  # girve prob for be mask
    model = get_model()
    X = []
    for img in masks:
        img = (central_img(img * 255, PHASE1_IMGSIZE_BEFORE_RESHAPE, final_size=PHASE1_IMGSIZE)) / 255.0;
        img = np.expand_dims(img, -1).astype(np.float32)
        X.append(img)
    X = np.array(X) 
    logits = model.predict_on_batch(X)
    probs = softmax(logits)
    return probs[:, 1]


def pahse2_get_user_lung_masks(uid, is_train):
    files = get_dcm_files(uid, is_train)
    masks = []
    rawimages = []
    for u in files:
        img = dicom_to_image(u, to_remove_board=False)
        rawimages.append(img)
    clip_positions = get_common_board(rawimages)
    for rawimg in rawimages:
        img = remove_board(rawimg, clip_positions)
        mask_list = phase2_make_lungmask(img)
        masks += mask_list
    return masks


def run():
    outputdir = config.OSIC_SANDBOX_HOME + "/train_phase2_masks/"
    if not os.path.exists(outputdir): os.mkdir(outputdir)
    
    users = get_users(True)
    
    for uid in tqdm(users[:]):
        output2 = outputdir + uid + '.mask.gz'
        if os.path.exists(output2): continue
        masks = pahse2_get_user_lung_masks(uid, True)
        pickle_dumpz(masks, output2)
    

if __name__ == '__main__':
    if 0:
        preds = phase2_predict([np.ones([20, 20]), np.ones([20, 20])])
        print(preds)
    if 1:
        import matplotlib.pyplot as plt
        # ID00367637202296290303449
        # ID00343637202287577133798
        # ID00255637202267923028520
        # ID00241637202264294508775
        # ID00048637202185016727717
        
        # img = dicom_to_image("../input/train/ID00255637202267923028520/16.dcm", to_remove_board=True)
        # img = dicom_to_image("../input/train/ID00367637202296290303449/130.dcm", to_remove_board=True)
        # img = dicom_to_image("../input/train/ID00343637202287577133798/20.dcm", to_remove_board=True)
        # img = dicom_to_image("../input/train/ID00241637202264294508775/128.dcm", to_remove_board=True)
        img = dicom_to_image("../input/train/ID00048637202185016727717/15.dcm", to_remove_board=True)
        plt.imshow(img)
        plt.show()
        phase2_make_lungmask(img, b_show=True)
    
    if 0:
        run() 
