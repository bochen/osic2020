'''
Created on Sep 2, 2020

@author: bo
'''

import networkx as nx    
from networkx.drawing.nx_pydot import graphviz_layout
import pandas as pd 
import numpy as np 
import yaml


def sae(x):
    m = np.median(x)
    return np.sum(np.abs(x - m))


def propose(df, candidate_columns, target):
    if len(df) == 0:
        return []
    l = []
    y = df[target].values
    y_loss = sae(y)
    for varname in candidate_columns:
        x = df[varname].values
        best_p = None
        max_diff = 0
        for point in np.unique(x):
            ly = y[x <= point]
            ry = y[x > point]
            # print(varname,point,len(ly),len(ry))
            if len(ly) < 10 or len(ry) <10:
                continue
            diff = y_loss - sae(ly) - sae(ry)
            if diff > max_diff:
                max_diff = diff
                best_p = (point, np.median(ly), np.median(ry))
        if best_p is not None:
            l.append([varname, best_p])
    return l

            
class NodeAttribute:
    
    def __init__(self, naid, split_id, varname:str, is_left:bool, propose_split_point, split_median):
        self.naid = naid
        self.split_id = split_id
        self.varname = varname
        self.is_left = is_left
        self.propose_split_point = propose_split_point
        self.split_median = split_median

    def apply(self, df):
        if self.varname is None:
            return df
        if self.is_left:
            return df[df[self.varname] <= self.point]
        else:
            return df[df[self.varname] > self.point]
    
    @property 
    def point(self):
        return float(self.propose_split_point)

    @property 
    def median(self):
        return float(self.split_median)
    
    def to_dict(self):
        r = {}
        r['id'] = self.naid
        r['split_id'] = self.split_id
        r['varname'] = self.varname 
        if self.varname is not None:
            r['dir'] = 'left' if self.is_left else 'right'
            r['point'] = self.point
            r['median'] = self.median
        return r 
            
    def __repr__(self):
        if self.varname is None:
            return "root"
        else:
            return "{}{}{:.4f}".format(self.varname, '<=' if self.is_left else '>', self.point)
    
    def propose_split(self, subdf, feats, target):
        splits = propose(subdf, feats, target)
        ret = []
        for s in splits:
            varname, point = s
            c1 = NodeAttribute("{}.{}".format(self.naid, 'l'), self.naid, varname, True, point[0], point[1])
            c2 = NodeAttribute("{}.{}".format(self.naid, 'r'), self.naid, varname, False, point[0], point[2])
            ret.append((c1, c2))
        return ret

    
class Tree:

    def __init__(self, df:pd.DataFrame, feats:list, target:str):
        self.graph = nx.DiGraph()
        self._nodeid_counter = 0
        self.df = df
        self.target = target
        self.feats = feats
        
        self.graph.add_node(self.next_id(), attrib=NodeAttribute('o', None, None, None, None, None))
    
    def to_dict(self):
        r = {}
        r['target'] = self.target 
        r['feats'] = self.feats 
        r['graph'] = self._node_to_dict(0)
        return r 

    def _node_to_dict(self, nid):
        r = {'id': nid, 'attrib': self.get_node_attrib(nid).to_dict()}
        children = self.get_children(nid)
        if children: 
            r['children'] = [self._node_to_dict(x) for x in children]
        return {'node': r}
    
    def to_yaml(self):
        return yaml.dump(self.to_dict(), default_flow_style=False)
    
    def next_id(self):
        i = self._nodeid_counter
        self._nodeid_counter += 1
        return i
    
    def copy(self):
        newtree = Tree(self.df, self.feats, self.target)
        newtree.graph = self.graph.copy()
        newtree._nodeid_counter = self._nodeid_counter
        return newtree
    
    def is_leaf(self, node):
        return len(self.graph.out_edges(node)) == 0

    def get_children(self, node):
        a = list(self.graph.out_edges(node))
        return [x[1] for x in a]
    
    def get_parent(self, node):
        a = list(self.graph.in_edges(node))
        assert len(a) < 2
        if len(a) == 1:
            return a[0][0]
        else:
            return None
    
    def get_subdf(self, nodeid , df):
        subdf = self.graph.nodes[nodeid]['attrib'].apply(df)
        p = self.get_parent(nodeid)
        if p is None:
            return subdf 
        else:
            return self.get_subdf(p, subdf)
    
    def get_node_attrib(self, nodeid):
        return self.graph.nodes[nodeid]['attrib']
    
    def split(self, df=None):
        
        ret = []
        for nodeid  in self.graph.nodes:
            if self.is_leaf(nodeid):
                subdf = self.get_subdf(nodeid, self.df)
                for c1, c2 in self.graph.nodes[nodeid]['attrib'].propose_split(subdf, self.feats, self.target):
                    newtree = self.copy()
                    newid = newtree.next_id()
                    newtree.graph.add_node(newid, attrib=c1)
                    newtree.graph.add_edge(nodeid, newid)
                    
                    newid = newtree.next_id()
                    newtree.graph.add_node(newid, attrib=c2)
                    newtree.graph.add_edge(nodeid, newid)
                    ret.append(newtree)
        return ret
    
    def draw(self, show_node=False):
        pos = graphviz_layout(self.graph, prog="dot")
        if show_node:
            nx.draw(self.graph, pos=pos, node_size=3000, node_color='white', with_labels=True)
        else:
            labels = nx.get_node_attributes(self.graph, 'attrib') 
            nx.draw(self.graph, pos=pos, labels=labels, node_size=3000, node_color='white')

        
if __name__ == '__main__':
    import matplotlib.pyplot as plt
    traindf = pd.DataFrame(np.random.random([100, 4]), columns=['c1', 'c2', 'c3', 'y'])
    t1 = Tree(traindf, ['c1', 'c2', 'c3'], 'y')
    a = t1.split()
    for t in a:
        print(t.to_yaml())
    k = len(a) // 2 + int(len(a) % 2 != 0)
    plt.figure(figsize=(20, 2 * k))
    for i, t in enumerate(a):
        plt.subplot(k, 2, i + 1)
        t.draw()
    plt.show()        
    
    a = t1.split()[0].split()[0].split()
    for t in a:
        print(t.to_yaml())
    
    k = len(a) // 2 + int(len(a) % 2 != 0)
    plt.figure(figsize=(20, 2 * k))
    for i, t in enumerate(a):
        plt.subplot(k, 2, i + 1)
        t.draw()
    plt.show()        
