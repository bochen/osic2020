'''
Created on Sep 1, 2020

@author: bo
'''
import config
import os
import pandas as pd 
import numpy as np 
from sklearn.preprocessing import StandardScaler
from sklearn.model_selection._split import GroupKFold

OSIC_INPUT_HOME = config.OSIC_INPUT_HOME

trainpath = os.path.join(OSIC_INPUT_HOME, 'train.csv')
testpath = os.path.join(OSIC_INPUT_HOME, 'test.csv')
submissionpath = os.path.join(OSIC_INPUT_HOME, 'sample_submission.csv')


def read_submission(path):
    submission_df = pd.read_csv(path)
    temp_sub_df = submission_df['Patient_Week'].str.split('_', expand=True)
    temp_sub_df.rename(columns={0: 'Patient', 1: 'Weeks'}, inplace=True)
    temp_sub_df['Weeks'] = temp_sub_df['Weeks'].astype(np.int)
    submission_df = pd.concat([submission_df, temp_sub_df], axis=1)
    submission_df = submission_df[['Patient', 'Weeks', 'Patient_Week']]
    return submission_df


def get_baseline(df):
    baseline = df[['Patient', 'Weeks', 'FVC', 'Percent']].sort_values(['Patient', 'Weeks']).groupby(['Patient']).first().reset_index()
    baseline.columns = ['Patient', 'base_Weeks', 'base_FVC', 'base_Percent']
    baseline['base_ref_FVC'] = baseline['base_FVC'].divide(baseline['base_Percent'] / 100)
    return baseline


def add_baseline(df):
    df = pd.merge(df, get_baseline(df), on='Patient', how='left')
    return df


def make_submission_df(submission_df, test_df):
    cols = set(test_df.columns).difference(submission_df.columns)
    cols = [u for u in cols if u not in ['FVC', 'Percent']]
    print(cols)
    df = pd.merge(submission_df, test_df[list(cols) + ["Patient"]], on='Patient', how='left')
    make_feature(df, is_train=False)
    return df


def make_feature(df, is_train):
    df['weeks_since_base'] = df['Weeks'] - df['base_Weeks']        
    if is_train:
        df['log_FVC'] = np.log(df['FVC'])
        df['log_base_FVC'] = np.log(df['base_FVC'])
        #df['FVC_excess'] = df['FVC'] - df['base_FVC']
        #df['FVC_ratio'] = df['FVC'] / df['base_FVC']
        pass 

    
def preprocess(df, onehot):
    sex = {'Male': 0, 'Female': 1}
    smoke = {'Ex-smoker': 1, 'Never smoked': 0, 'Currently smokes': 2}
    df['Sex'] = df['Sex'].map(sex)
    df['SmokingStatus'] = df['SmokingStatus'].map(smoke)
    if True:
        df['SmokingStatus_1'] = (df['SmokingStatus'] == 1).astype(np.int)
        df['SmokingStatus_2'] = (df['SmokingStatus'] == 2).astype(np.int)
    return df


def expand_train(df):
    
    df = df.drop(['base_Weeks', 'base_FVC', 'base_Percent'], axis=1)
    dynamic_cols = ['Weeks', 'FVC', 'Percent'] 
    static_df = df.loc[:, ~df.columns.isin(dynamic_cols)].drop_duplicates()
    dynamic_df = df[["Patient"] + dynamic_cols].sort_values(['Patient', 'Weeks'])
    lst = []
    for patient in set(df['Patient'].values):
        subdf = dynamic_df[dynamic_df['Patient'] == patient]
        for i in range(subdf.shape[0]):
            lst.append(subdf.iloc[i].to_list() + subdf.iloc[i].to_list()[1:])
                    
        for i in range(subdf.shape[0]):
            for j in range(subdf.shape[0]):
                if i != j:
                    lst.append(subdf.iloc[j].to_list() + subdf.iloc[i].to_list()[1:])
    newdf = pd.DataFrame(lst, columns=['Patient', 'Weeks', 'FVC', 'Percent'] + ['base_Weeks', 'base_FVC', 'base_Percent'])
    
    newdf = pd.merge(newdf, static_df, how='left', on='Patient')
    return newdf
    

def _read_tabular_data(path, is_train=False, train_expand=True, onehot=True):
    df = pd.read_csv(path)
    if is_train:
        df = df.groupby(['Patient', 'Weeks']).aggregate({'FVC':'mean', 'Percent':'mean', 'Age':'first', 'Sex':'first', 'SmokingStatus':'first'}).reset_index()
    else:
        assert df.shape[0] == df.groupby(['Patient'])['Age'].count().shape[0]
    df = preprocess(df, onehot=onehot) 
    df = add_baseline(df)
    if is_train and train_expand:
        df = expand_train(df)
        
    make_feature(df, is_train)
    return df


standardized_columns = ['FVC', 'Age', 'base_Percent', 'base_FVC', 'base_ref_FVC', 'weeks_since_base']


def make_standard_scaler(rawtriandf, rawtestdf):
    df = pd.concat([rawtriandf, rawtestdf])
    d = {}
    for col in standardized_columns:
        scaler = StandardScaler().fit(df[col].values.reshape(-1, 1))
        d[col] = scaler
    return d


scalers = None


def remove_train_outliers(traindf):
    from sklearn.linear_model import Ridge

    def remove_outlier(traindf):
        cols=['Age', 'Sex', 'SmokingStatus_1', 'SmokingStatus_2', 'base_FVC',
       'base_Percent', 'weeks_since_base']
        X_train = traindf[cols].values
        y_train = traindf['FVC'].values
        model = Ridge()
        model.fit(X_train, y_train)
        y_pred = model.predict(X_train)
        err = (y_train - y_pred)
        rmse = np.sqrt(np.mean((err) ** 2))
        print('rmse:', rmse, 'mean err:', np.mean(err))
        idx = np.where(np.abs(err) > 3 * rmse, 0, 1)
        return traindf.loc[idx > 0]

    return remove_outlier(traindf)

    
def get_data(train_expand=True, return_normalized=False, remove_outlier=True, onehot=True, after_base_line=True):
    rawtraindf = _read_tabular_data(trainpath, is_train=True, train_expand=False, onehot=onehot)
    traindf = _read_tabular_data(trainpath, is_train=True, train_expand=train_expand, onehot=onehot)
    if after_base_line:
        traindf=traindf[traindf['weeks_since_base']>0]
    testdf = _read_tabular_data(testpath, train_expand=False, onehot=onehot)
    submissiondf = read_submission(submissionpath)
    submissiondf = make_submission_df(submissiondf, testdf)
    
    if return_normalized:
        global scalers
        scalers = make_standard_scaler(rawtraindf, testdf)        
        for col in standardized_columns:
            traindf['norm_' + col] = scalers[col].transform(traindf[col].values.reshape(-1, 1))
            if col not in ['FVC', 'FVC_ratio', 'FVC_excess']:
                testdf['norm_' + col] = scalers[col].transform(testdf[col].values.reshape(-1, 1))

        submissiondf['norm_' + 'weeks_since_base'] = scalers[col].transform(submissiondf['weeks_since_base'].values.reshape(-1, 1))
    
    if remove_outlier:
        traindf = remove_train_outliers(traindf)
        
    return rawtraindf, traindf, testdf, submissiondf    
        

def make_kfold(train_df, n_split=5):
    group_kfold = GroupKFold(n_splits=n_split)
    groups = train_df['Patient'].values 
    folds = []
    for train_index, test_index in group_kfold.split(train_df['Patient'].values, groups=groups):
        folds.append((train_df.iloc[train_index], train_df.iloc[test_index]))
    return folds
