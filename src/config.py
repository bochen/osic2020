'''
Created on Aug 13, 2020

@author: bo
'''

import os 

if 'KAGGLE_ENV' in os.environ:
    OSIC_HOME = "/kaggle/input/"
    OSIC_INPUT_HOME = "/kaggle/input/osic-pulmonary-fibrosis-progression"
    OSIC_SANDBOX_HOME = "/tmp"
else:
    OSIC_HOME = os.path.join(os.environ['HOME'],'mydev','osic2020')
    OSIC_INPUT_HOME = os.path.join(os.environ['HOME'],'mydev','osic2020','input')
    OSIC_SANDBOX_HOME = os.path.join(OSIC_HOME, 'sandbox')



lungmask_modelfile='https://github.com/JoHof/lungmask/releases/download/v0.0/unet_r231-d5d2fc3d.pth'
if 'KAGGLE_ENV' in os.environ:
    lungmask_modelfile='file:/kaggle/input/unet-r231/unet_r231-d5d2fc3d.pth'


def create_folder_if_not_exists(path):
    if not os.path.exists(path):
        os.mkdir(path)


