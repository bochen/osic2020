'''
Created on Aug 13, 2020

@author: bo
'''

# Segmentation
from skimage import morphology
from skimage import measure
from sklearn.cluster import KMeans
import numpy as np 
import config
import glob
import os
import gzip

OSIC_INPUT_HOME = config.OSIC_INPUT_HOME

import pydicom


def fix_pxrepr(dcm):
    if dcm.PixelRepresentation != 0 or dcm.RescaleIntercept < -100: return
    x = dcm.pixel_array + 1000
    px_mode = 4096
    x[x >= px_mode] = x[x >= px_mode] - px_mode
    dcm.PixelData = x.tobytes()
    dcm.RescaleIntercept = -1000


def remove_board(img, clip_positions=None):
    if clip_positions is None:
        x1, x2, y1, y2 = get_board(img)
    else:
        x1, x2, y1, y2 = clip_positions
    img = img[y1:y2, x1:x2]
    return img


def get_board(img):
    a = img.sum(0)
    idx = np.where(a != 0)[0]
    if len(idx) == 0:
        x1, x2 = 0, 0
    else:
        x1, x2 = idx[0], idx[-1] + 1

    a = img.sum(1)
    idx = np.where(a != 0)[0]
    if len(idx) == 0:
        y1, y2 = 0, 0
    else:
        y1, y2 = idx[0], idx[-1] + 1    
    return (x1, x2, y1, y2)


def dicom_to_image(filename, to_remove_board=True):
    im = pydicom.dcmread(filename)
    fix_pxrepr(im)
    img = im.pixel_array
    img[img == -2000] = 0
    if to_remove_board:
        return remove_board(img)
    else:
        return img

# https://www.raddq.com/dicom-processing-segmentation-visualization-in-python/


def make_lungmask(img, display=False, fun_process_labels=None):
    row_size = img.shape[0]
    col_size = img.shape[1]
    
    mean = np.mean(img)
    std = np.std(img) + 1e-7
    img = img - mean
    img = img / std
    
    # Find the average pixel value near the lungs
        # to renormalize washed out images
    middle = img[int(col_size / 5):int(col_size / 5 * 4), int(row_size / 5):int(row_size / 5 * 4)] 
    mean = np.mean(middle)  
    max = np.max(img)
    min = np.min(img)
    
    # To improve threshold finding, I'm moving the 
    # underflow and overflow on the pixel spectrum
    img[img == max] = mean
    img[img == min] = mean
    
    # Using Kmeans to separate foreground (soft tissue / bone) and background (lung/air)
    
    kmeans = KMeans(n_clusters=2).fit(np.reshape(middle, [np.prod(middle.shape), 1]))
    centers = sorted(kmeans.cluster_centers_.flatten())
    threshold = np.mean(centers)
    thresh_img = np.where(img < threshold, 1.0, 0.0)  # threshold the image

    # First erode away the finer elements, then dilate to include some of the pixels surrounding the lung.  
    # We don't want to accidentally clip the lung.

    eroded = morphology.erosion(thresh_img, np.ones([3, 3]))
    dilation = morphology.dilation(eroded, np.ones([8, 8]))

    labels = measure.label(dilation)  # Different labels are displayed in different colors
    label_vals = np.unique(labels)
    regions = measure.regionprops(labels)
    if fun_process_labels is not None:  # original method
        return fun_process_labels(img, labels, regions)        
    else:
        good_labels = []
        for prop in regions:
            B = prop.bbox
    #         print(B,B[2] - B[0] , row_size / 10 * 9 , B[3] - B[1] , col_size / 10 * 9 ,
    #                  B[0] , row_size / 5 , B[2] , col_size / 5 * 4)
            if B[2] - B[0] < row_size / 10 * 9 and B[3] - B[1] < col_size / 10 * 9 \
                    and B[0] > row_size * 0.15 and B[2] < col_size * 0.85:
                good_labels.append(prop.label)
        
        mask = np.ndarray([row_size, col_size], dtype=np.int8)
        mask[:] = 0
    
        #  After just the lungs are left, we do another large dilation
        #  in order to fill in and out the lung mask 
        
        for N in good_labels:
            mask = mask + np.where(labels == N, 1, 0)
        mask = morphology.dilation(mask, np.ones([10, 10]))  # one last dilation
    
        if (display):
            import matplotlib.pyplot as plt 
            fig, ax = plt.subplots(3, 2, figsize=[12, 12])
            ax[0, 0].set_title("Original")
            ax[0, 0].imshow(img, cmap='gray')
            ax[0, 0].axis('off')
            ax[0, 1].set_title("Threshold")
            ax[0, 1].imshow(thresh_img, cmap='gray')
            ax[0, 1].axis('off')
            ax[1, 0].set_title("After Erosion and Dilation")
            ax[1, 0].imshow(dilation, cmap='gray')
            ax[1, 0].axis('off')
            ax[1, 1].set_title("Color Labels")
            ax[1, 1].imshow(labels)
            ax[1, 1].axis('off')
            ax[2, 0].set_title("Final Mask")
            ax[2, 0].imshow(mask, cmap='gray')
            ax[2, 0].axis('off')
            ax[2, 1].set_title("Apply Mask on Original")
            ax[2, 1].imshow(mask * img, cmap='gray')
            ax[2, 1].axis('off')
            
            plt.show()
        return mask


def get_users(is_train):
    if is_train:
        folder = os.path.join(OSIC_INPUT_HOME, 'train')
    else:
        folder = os.path.join(OSIC_INPUT_HOME, 'test')
    from os import listdir
    from os.path import isdir, join
    users = [f for f in listdir(folder) if isdir(join(folder, f))]
    return users


def get_dcm_files(uid, is_train):
    if is_train:
        folder = os.path.join(OSIC_INPUT_HOME, 'train')
    else:
        folder = os.path.join(OSIC_INPUT_HOME, 'test')

    files = glob.glob("{}/{}/*.dcm".format(folder, uid))
    files = sorted(files, key=lambda u: int(u.split("/")[-1].split(".")[0]))
    return files 


def get_common_board(images):
    pos = []
    for img in images:
        pos.append(get_board(img))
    pos = np.median(np.array(pos), axis=0).astype(np.int)
    return pos 


def get_user_lungs(uid, is_train):
    files = get_dcm_files(uid, is_train)
    masks = []
    lungs = []
    rawimages = []
    for u in files:
        img = dicom_to_image(u, to_remove_board=False)
        rawimages.append(img)
    clip_positions = get_common_board(rawimages)
    for rawimg in rawimages:
        img = remove_board(rawimg, clip_positions)
        mask = make_lungmask(img, display=False)
        masks.append(mask)
        lungs.append(img * mask)
    return np.array(masks, dtype=np.int8), np.array(lungs, dtype=np.int32)


def save_array_z(arr, filepath):
    with gzip.GzipFile(filepath, "w", compresslevel=1) as f:
        np.save(file=f, arr=arr)


def load_array_z(filepath):
    with gzip.GzipFile(filepath, "r") as f:
        return np.load(file=f)
    

def _get_slices_thickness(slices):
    x = []
    for s in slices:
        if hasattr(s, 'SliceThickness'):
            x.append(float(s.SliceThickness))
            # x.append(float(s['0018', '0050'].value))
    if len(x) * 2 > len(slices):
        return np.median(x)
        
    x = []
    for i in range(1, len(slices)):
        if hasattr(slices[i - 1], 'ImagePositionPatient') and hasattr(slices[i], 'ImagePositionPatient'):
            x.append(np.abs(slices[i - 1].ImagePositionPatient[2] - slices[i].ImagePositionPatient[2]))
            # x.append(np.abs(slices[i-1]['0020', '0032'].value[2] - slices[i]['0020', '0032'].value[2]))
    if len(x) * 2 > len(slices):
        return np.median(x)        
    
    x = []
    for i in range(1, len(slices)):
        if hasattr(slices[i - 1], 'SliceLocation') and hasattr(slices[i], 'SliceLocation'):
            # x.append(np.abs(float(slices[i-1]['0020', '1041'].value) - float(slices[i]['0020', '1041'].value)))
            x.append(np.abs(slices[i - 1].SliceLocation - slices[i].SliceLocation))
    if len(x) * 2 > len(slices):
        return np.median(x)        
    
    return None    


def _get_slices_depth(slices):
    x = []
    for s in slices:
        if hasattr(s, 'SliceThickness') and hasattr(s, "SpacingBetweenSlices"):
            x.append(float(s.SliceThickness)+float(s.SpacingBetweenSlices))
            # x.append(float(s['0018', '0050'].value))
    if len(x) * 2 > len(slices):
        return np.median(x)
        
    x = []
    for i in range(1, len(slices)):
        if hasattr(slices[i - 1], 'ImagePositionPatient') and hasattr(slices[i], 'ImagePositionPatient'):
            x.append(np.abs(slices[i - 1].ImagePositionPatient[2] - slices[i].ImagePositionPatient[2]))
            # x.append(np.abs(slices[i-1]['0020', '0032'].value[2] - slices[i]['0020', '0032'].value[2]))
    if len(x) * 2 > len(slices):
        return np.median(x)        
    
    x = []
    for i in range(1, len(slices)):
        if hasattr(slices[i - 1], 'SliceLocation') and hasattr(slices[i], 'SliceLocation'):
            # x.append(np.abs(float(slices[i-1]['0020', '1041'].value) - float(slices[i]['0020', '1041'].value)))
            x.append(np.abs(slices[i - 1].SliceLocation - slices[i].SliceLocation))
    if len(x) * 2 > len(slices):
        return np.median(x)        
    
    return None    


def _get_slices_spacing(slices):
    x = []
    for s in slices:
        if hasattr(s,  'SpacingBetweenSlices'):
            x.append(float(s.SpacingBetweenSlices))
            # x.append(float(s['0018', '0050'].value))
    if x:
        return np.median(x)
    
    return None    

def _get_pixel_spacing(slices):
    x = []
    for s in slices:
        if hasattr(s,  'PixelSpacing'):
            x.append(np.array(s.PixelSpacing))
            # x.append(float(s['0018', '0050'].value))
    if x:
        return np.median(x,axis=0)
    
    return None    

        
def scan_thickness(uid, is_train):
    files = get_dcm_files(uid, is_train)
    
    slices = [pydicom.dcmread(fname, defer_size=100, stop_before_pixels=True, force=True) for fname in files]
    return _get_slices_thickness(slices), _get_slices_spacing(slices) , _get_slices_depth(slices), _get_pixel_spacing(slices)


if __name__ == '__main__':
    if 1:
        import matplotlib.pyplot as plt
        img = dicom_to_image("../input/train/ID00241637202264294508775/128.dcm", to_remove_board=True)
        plt.imshow(img)
        plt.show()
        make_lungmask(img,display=True)
    
        
