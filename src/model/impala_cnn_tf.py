from model.framework import try_import_tf

tf = try_import_tf()


def conv_layer(depth, name):
    return tf.keras.layers.Conv2D(
        filters=depth, kernel_size=3, strides=1, padding="same", name=name
    )


def residual_block(x, depth, prefix):
    inputs = x
    assert inputs.get_shape()[-1] == depth
    x = tf.keras.layers.ReLU()(x)
    x = conv_layer(depth, name=prefix + "_conv0")(x)
    x = tf.keras.layers.ReLU()(x)
    x = conv_layer(depth, name=prefix + "_conv1")(x)
    return x + inputs


def conv_sequence(x, depth, prefix):
    x = conv_layer(depth, prefix + "_conv")(x)
    x = tf.keras.layers.MaxPool2D(pool_size=3, strides=2, padding="same")(x)
    x = residual_block(x, depth, prefix=prefix + "_block0")
    x = residual_block(x, depth, prefix=prefix + "_block1")
    return x



def make_model(input_shape=(224,224,1), num_outputs=2):

    depths = [16, 32, 32, 32]

    inputs = tf.keras.layers.Input(shape=input_shape, name="observations")
    scaled_inputs = tf.cast(inputs, tf.float32) / 255.0

    x = scaled_inputs
    for i, depth in enumerate(depths):
        x = conv_sequence(x, depth, prefix=f"seq{i}")

    x = tf.keras.layers.Flatten()(x)
    x = tf.keras.layers.ReLU()(x)
    x = tf.keras.layers.Dense(units=256, activation="relu", name="hidden")(x)
    logits = tf.keras.layers.Dense(units=num_outputs, name="pi")(x)

    model = tf.keras.Model(inputs=inputs, outputs=logits, name="my_model")
    return model


