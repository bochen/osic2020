'''
Created on Aug 16, 2020

@author: bo
'''

import pickle 
import gzip


def pickle_dumpz(obj, filepath):
    with gzip.open(filepath, 'w') as f:
        pickle.dump(obj, f)

        
def pickle_loadz(filepath):
    with gzip.open(filepath, 'r') as f:
        return pickle.load(f)        
