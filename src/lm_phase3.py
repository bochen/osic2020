'''
Created on Aug 13, 2020

@author: bo
'''

# Segmentation
import numpy as np 
import config
from model.impala_cnn_tf import make_model
import phase1_mask_model
import phase2_mask_model  
from phase1_mask_model import     central_img    
from lm_andrada import dicom_to_image, remove_board, make_lungmask, get_users, \
    get_dcm_files, get_common_board, save_array_z

from tqdm import tqdm 
import os
from utils import pickle_dumpz
from lm_phase2 import _get_crop, softmax

OSIC_INPUT_HOME = config.OSIC_INPUT_HOME


def _phase1_filter(labeled_img, good_regions, good_masks0, rawimg, good_images0, threshold=0.08):
    good_index = []
    if len(good_masks0) > 0:
        probs = phase1_predict(good_masks0)
        for i, p in enumerate(probs):
            if p > threshold:
                good_index.append(i)
    good_masks = []
    good_images = []
    for i in good_index:
        good_masks.append(good_masks0[i])
        good_images.append(good_images0[i])
    for i in good_index:
        for j in good_index:
            if(i < j):
                a = _get_crop(labeled_img, [good_regions[i], good_regions[j]], rawimg=rawimg)
                good_masks.append(a[0])
                good_images.append(a[1])
    return good_masks, good_images


def _phase1_filter_v2(labeled_img, good_regions, good_masks0, rawimg, good_images0):
    areas = [prop.area for prop in good_regions]
    idx = np.argsort(areas)[::-1]
    #good_index = [u for u in idx if _phase3_image_stat_filter(good_masks0[u], good_images0[u])]
    #good_index = good_index[:10]
    good_index = list(idx)[:10]
    good_masks = []
    good_images = []
    for i in good_index:
        good_masks.append(good_masks0[i])
        good_images.append(good_images0[i])
    for i in good_index:
        for j in good_index:
            if(i < j):
                a = _get_crop(labeled_img, [good_regions[i], good_regions[j]], rawimg=rawimg)
                good_masks.append(a[0])
                good_images.append(a[1])
    return good_masks, good_images


def _phase2_filter(good_masks1, good_images1, threshold=0.8):
    good_index = []
    if len(good_masks1) > 0:
        probs = phase2_predict(good_masks1)
        for i, p in enumerate(probs):
            if p > threshold:
                good_index.append([i, p])
    if(good_index):
        good_index = sorted(good_index, key=lambda u: u[1])
        i, p = good_index[-1]
        return  [ (p, good_masks1[i], good_images1[i]) ]  # #should only at most have one logically
    else:
        return []

    
def _phase3_image_stat_filter(mask, img):
    MEAN = -0.171028   
    MEAN_STD = 0.282453  
    STD = 0.508788     
    STD_STD = 0.146441
    
    mean = (img).sum() / mask.sum()
    img = (img - mean) * mask
    var = (img * img).sum() / mask.sum()
    std = np.sqrt(var)
    
    return (mean <= MEAN + 3 * MEAN_STD and mean >= MEAN - 3 * MEAN_STD) and (std <= STD + 3 * STD_STD and std >= STD - 3 * STD_STD)


def phase3_fun_to_get_good_labels(img, labeled_img, old_regions, b_show=False):
    assert img.shape == labeled_img.shape
    w, h = img.shape 
    fun0 = lambda B, row_size, col_size: B[2] - B[0] < row_size / 10 * 9 and B[3] - B[1] < col_size / 10 * 9 
    
    fun = lambda B, row_size, col_size: B[2] - B[0] < row_size / 10 * 9 and B[3] - B[1] < col_size / 10 * 9 \
                    and B[0] > row_size * 0.15 and B[2] < col_size * 0.85
    area = w * h
    good_masks0 = []
    good_images0 = []
    good_regions = []
    
    regions = [x for x in old_regions if fun0(x.bbox, w, h)]
    if len(regions) == 0: return []
    
    max_region_area = np.max([x.area for x in regions])
    for prop in regions:
        # if prop.area / area > 0.03 or fun(prop.bbox, w,h):
        if prop.area / area > 0.01 and prop.area / max_region_area > 0.10 or fun(prop.bbox, w, h):
            a = _get_crop(labeled_img, prop, rawimg=img)
            good_masks0.append(a[0])
            good_images0.append(a[1])
            good_regions.append(prop)
    
    if 0:
        good_masks1, good_images1 = _phase1_filter(labeled_img, good_regions, good_masks0, img, good_images0)
        good_masks = _phase2_filter(good_masks1, good_images1)
    else:
        good_masks1, good_images1 = _phase1_filter_v2(labeled_img, good_regions, good_masks0, img, good_images0)
        good_masks = _phase2_filter(good_masks1, good_images1)
    
    if b_show:
        import matplotlib.pyplot as plt
        for p, mask, mask_img in good_masks:
            plt.subplot(1, 2, 1)
            plt.imshow(mask,cmap='gray')
            plt.subplot(1, 2, 2)
            plt.imshow(mask_img,cmap='gray')
            plt.title("Prob=" + str(p))
            plt.show()
    return good_masks


def phase3_make_lungmask(img, b_show=False):
    return make_lungmask(img, fun_process_labels=lambda *args: phase3_fun_to_get_good_labels(*args, b_show=b_show))
 

_phase1_model = None
_phase2_model = None  


def get_phase1_model():
    global _phase1_model
    if(_phase1_model is None):
        _phase1_model = make_model(input_shape=(*phase1_mask_model.PHASE1_IMGSIZE, 1), num_outputs=2)
        _phase1_model.load_weights(os.path.join(config.OSIC_HOME, 'phase1_mask', 'phase1_mask'))
    return _phase1_model


def get_phase2_model():
    global _phase2_model
    if(_phase2_model is None):
        _phase2_model = make_model(input_shape=(*phase2_mask_model.PHASE1_IMGSIZE, 1), num_outputs=2)
        _phase2_model.load_weights(os.path.join(config.OSIC_HOME, 'phase2_mask', 'phase2_mask'))
    return _phase2_model


def phase1_predict(masks):  # girve prob for be mask
    model = get_phase1_model()
    X = []
    for img in masks:
        img = (central_img(img * 255, phase1_mask_model.PHASE1_IMGSIZE_BEFORE_RESHAPE, final_size=phase1_mask_model.PHASE1_IMGSIZE)) / 255.0;
        img = np.expand_dims(img, -1).astype(np.float32)
        X.append(img)
    X = np.array(X) 
    logits = model.predict_on_batch(X)
    probs = softmax(logits)
    return probs[:, 1]


def phase2_predict(masks):  # girve prob for be mask
    model = get_phase2_model()
    X = []
    for img in masks:
        img = (central_img(img * 255, phase2_mask_model.PHASE1_IMGSIZE_BEFORE_RESHAPE, final_size=phase2_mask_model.PHASE1_IMGSIZE)) / 255.0;
        img = np.expand_dims(img, -1).astype(np.float32)
        X.append(img)
    X = np.array(X) 
    logits = model.predict_on_batch(X)
    probs = softmax(logits)
    return probs[:, 1]


def pahse3_get_user_lung_masks(uid, is_train):
    files = get_dcm_files(uid, is_train)
    masks = []
    rawimages = []
    file_index = []
    for u in files:
        file_index.append(int(u.split("/")[-1].split('.')[0]))
        img = dicom_to_image(u, to_remove_board=False)
        rawimages.append(img)
    clip_positions = get_common_board(rawimages)
    for idx, rawimg in zip(file_index, rawimages):
        img = remove_board(rawimg, clip_positions)
        mask_list = phase3_make_lungmask(img)
        masks += [ (idx, u) for u in  mask_list]
    return masks


def run():
    outputdir = config.OSIC_SANDBOX_HOME + "/train_phase3_masks/"
    if not os.path.exists(outputdir): os.mkdir(outputdir)
    
    users = get_users(True)
    
    for uid in tqdm(users[:]):
        output2 = outputdir + uid + '.mask.gz'
        if os.path.exists(output2): continue
        masks = pahse3_get_user_lung_masks(uid, True)
        pickle_dumpz(masks, output2)
    

if __name__ == '__main__':
    if 0:
        preds = phase2_predict([np.ones([20, 20]), np.ones([20, 20])])
        print(preds)
    if 0:
        import matplotlib.pyplot as plt
        # img = dicom_to_image("../input/train/ID00255637202267923028520/16.dcm", to_remove_board=True)
        # img = dicom_to_image("../input/train/ID00367637202296290303449/130.dcm", to_remove_board=True)
        # img = dicom_to_image("../input/train/ID00343637202287577133798/20.dcm", to_remove_board=True)
        # img = dicom_to_image("../input/train/ID00241637202264294508775/128.dcm", to_remove_board=True)
        # img = dicom_to_image("../input/train/ID00048637202185016727717/14.dcm", to_remove_board=True)
        img = dicom_to_image("../input/train/ID00165637202237320314458/11.dcm", to_remove_board=True)

        plt.imshow(img,cmap='gray')
        plt.show()
        phase3_make_lungmask(img, b_show=True)
    if 1:
        run() 
