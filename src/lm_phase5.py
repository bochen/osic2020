'''
Created on Aug 13, 2020

@author: bo
'''

# Segmentation
import numpy as np 
import config
from lm_andrada import remove_board, get_users, \
    get_dcm_files, get_common_board

from tqdm import tqdm 
import os
from utils import pickle_dumpz
import cv2 
import pydicom
try:
    from ckmeans import ckmeans
except:
    pass

OSIC_INPUT_HOME = config.OSIC_INPUT_HOME


def refine_input(im_th):
    cnts, hir = cv2.findContours(im_th, cv2.RETR_TREE, cv2.CHAIN_APPROX_NONE)
    areas = [cv2.contourArea(cnt) for cnt in cnts] 
    if len(areas) == 0: return im_th
    i = np.argsort(areas)[-1]
    cnt = cnts[i]
    a = np.zeros([*im_th.shape, 3], dtype=np.uint8)
    cv2.drawContours(a, cnts, i, (255, 255, 255), thickness=cv2.FILLED);
    mask = (a[:, :, 0] > 0).astype(np.uint8)
    kernel = cv2.getStructuringElement(cv2.MORPH_ELLIPSE, (3, 3))
    mask = cv2.dilate(mask, kernel, iterations=20)    
    return (mask > 0) * im_th

    
def refine_mask(mask):
    kernel = cv2.getStructuringElement(cv2.MORPH_ELLIPSE, (3, 3))
    mask = cv2.dilate(mask, kernel, iterations=20)

    cnts, hir = cv2.findContours(255 - mask * 255, cv2.RETR_TREE, cv2.CHAIN_APPROX_NONE)
    areas = [cv2.contourArea(cnt) for cnt in cnts] 
    if len(areas) == 0: return mask
    # if len(areas)==0: return mask*0
    i = np.argsort(areas)[-1]
    cnt = cnts[i]
    a = np.zeros([*mask.shape, 3], dtype=np.uint8)
    cv2.drawContours(a, cnts, i, (255, 255, 255), thickness=cv2.FILLED);
    return a[:, :, 0]


def flushing(im_th, b_show=False):
    im_th = refine_input(im_th)
    im_floodfill = im_th.astype(np.uint8).copy() 
    h, w = im_th.shape[:2]
    mask = np.zeros((h + 2, w + 2), np.uint8)
    cv2.floodFill(im_floodfill, mask, (0, 0), 255);
    cv2.floodFill(im_floodfill, mask, (w - 1, 0), 255);
    cv2.floodFill(im_floodfill, mask, (0, h - 1), 255);
    cv2.floodFill(im_floodfill, mask, (w - 1, h - 1), 255);
    mask = refine_mask(mask)
    im_floodfill_inv = cv2.bitwise_not(im_floodfill)
    mask = mask[1:-1, 1:-1]
    im_out = mask * im_floodfill_inv
    if b_show:
        import matplotlib.pyplot as plt
        plt.subplot(2, 2, 1)
        plt.imshow(im_th, cmap='gray')  
        plt.axis('off')
        plt.title("orignal")
        plt.subplot(2, 2, 2)
        plt.axis('off')
        plt.imshow(mask, cmap='gray')    
        plt.title("mask")
        plt.subplot(2, 2, 3)
        plt.axis('off')
        plt.imshow(im_floodfill, cmap='gray')    
        plt.title("floodfill")
        plt.subplot(2, 2, 4)
        plt.axis('off')
        plt.imshow(im_out, cmap='gray')    
        plt.title("out")
        plt.show()
    return im_out, mask


def th(img, low, high):
    a = img.copy() 
    a[a < low] = low
    a[a > high] = high

    a = (a - low) / (a.max() - a.min() + 1e-7)

    im_th1 = ((a > 0.5) * 255).astype(np.uint8)
    return im_th1


def th3(img, threshold):
    a = img.copy() 
    a[img > threshold] = 1
    a[img <= threshold] = 0
    im_th1 = (a).astype(np.uint8)
    return im_th1


def fix_dicom_by_check_bins(img):
    a = img.reshape(-1)
    i = [-3500, -2500, -1500, -500, np.inf]
    j = [-np.inf, -3500, -2500, -1500, -500]
    bins = []
    N = img.shape[0] * img.shape[1]
    for l, h in zip(j, i):
        n = ((img < h) & (img >= l)).sum() / N
        if n > 0.05:
            bins.append([(l, h), n])
    
    bins = dict(bins)
    if len(bins) == 1: return img
    assert len(bins) in [2, 3]
    
    if len(bins) == 2:
        if (-2500, -1500) in bins and (-1500, -500) in bins:
            return img + 1000
        else:
            return img
    else:
        if (-np.inf, -3500) in bins:
            newimg = img + 2000
            newimg[img < -3500 ] = -1000
            return newimg
        elif (-3500, -2500) in bins:
            newimg = img.copy()
            newimg[img < -2500] = -1000
            return newimg    
        elif (-2500, -1500) in bins:
            newimg = img.copy()
            newimg[img < -1500] = -1000
            return newimg    
        else:
            print(bins)
            raise Exception("something else")


fix_dicom_img = fix_dicom_by_check_bins 


def fix_dicom_img_bk(img):
    res = ckmeans(img.reshape(-1), (2, 3))
    
    if res.centers[0] < -3500:
         if np.abs(3000 + res.centers[1]) < 100 and np.abs(2000 + res.centers[2]) < 100:
            clustering = res.clustering.reshape(img.shape)
            newimg = img + 2000
            newimg[clustering == 0] = -1000
            return newimg
         else:
            print(res.centers)
            print(res.sizes)
            raise Exception("something else")        
    elif res.centers[0] < -2500:
        clustering = res.clustering.reshape(img.shape)
        newimg = img.copy()
        newimg[clustering == 0] = -1000
        return newimg
    else:
        if np.abs(1000 + res.centers[0]) < 100 and np.abs(0 + res.centers[1]) < 100:
            return img
        else:
            print(res.centers)
            print(res.sizes)
            raise Exception("something else")


def dicom_to_image(filename, to_remove_board=True):
    im = pydicom.dcmread(filename)
    slop = im.RescaleSlope
    intercept = im.RescaleIntercept
    pixel_spacing = im.PixelSpacing
    pixel_spacing = pixel_spacing[0] * pixel_spacing[1]
    
    img = im.pixel_array
    img = img * slop + intercept
    img = fix_dicom_img(img)
    if to_remove_board:
        return remove_board(img), pixel_spacing
    else:
        return img, pixel_spacing


def phase4_make_lungmask(img, b_show=False):
    im_th1 = th(img, -600, -600 + 1)
    _, mask = flushing(im_th1, b_show)
    img = img * (mask > 0)
    im_th2 = 1 - th3(img, -500)
    return [im_th2]


def pahse4_get_user_lung_masks(uid, is_train):
    files = get_dcm_files(uid, is_train)
    masks = []
    rawimages = []
    file_index = []
    pixel_spacings = []
    for u in files:
        file_index.append(int(u.split("/")[-1].split('.')[0]))
        # #print(u)
        img, ps = dicom_to_image(u, to_remove_board=False)
        pixel_spacings.append(ps)
        rawimages.append(img)
    clip_positions = get_common_board(rawimages)
    for idx, rawimg, ps in zip(file_index, rawimages, pixel_spacings):
        # print(uid,idx)
        img = remove_board(rawimg, clip_positions)
        mask_list = phase4_make_lungmask(img)
        masks += [ (idx, u, ps) for u in  mask_list]
    return masks


def run():
    outputdir = config.OSIC_SANDBOX_HOME + "/train_phase5_masks/"
    if not os.path.exists(outputdir): os.mkdir(outputdir)
    
    users = get_users(True)
    
    for uid in tqdm(users[:]):
        output2 = outputdir + uid + '.mask.gz'
        if os.path.exists(output2): continue
        masks = pahse4_get_user_lung_masks(uid, True)
        pickle_dumpz(masks, output2)
    

if __name__ == '__main__':
    if 0:
        import matplotlib.pyplot as plt
        # img = dicom_to_image("../input/train/ID00255637202267923028520/16.dcm", to_remove_board=True)
        # img = dicom_to_image("../input/train/ID00367637202296290303449/130.dcm", to_remove_board=True)
        # img = dicom_to_image("../input/train/ID00343637202287577133798/20.dcm", to_remove_board=True)
        # img = dicom_to_image("../input/train/ID00241637202264294508775/128.dcm", to_remove_board=True)
        # img = dicom_to_image("../input/train/ID00048637202185016727717/14.dcm", to_remove_board=True)
        # img = dicom_to_image("../input/train/ID00165637202237320314458/11.dcm", to_remove_board=True)
        # img = dicom_to_image("../input/train/ID00381637202299644114027/31.dcm", to_remove_board=True)
        img, _ = dicom_to_image("../input/train/ID00132637202222178761324/1.dcm", to_remove_board=True)

        plt.imshow(img, cmap='gray')
        plt.show()
        phase4_make_lungmask(img, b_show=True)
    else:
        run() 
