'''
Created on Aug 13, 2020

@author: bo
'''

from lungmask import mask
from lungmask.resunet import UNet
import pydicom as dcm
import SimpleITK as sitk
import torch
import glob
import gzip
import numpy as np 
import config
import os

OSIC_INPUT_HOME = config.OSIC_INPUT_HOME

lungmask_modelfile = config.lungmask_modelfile


def get_model(model_url, modeltype='unet'):
    n_classes = 3
    state_dict = torch.hub.load_state_dict_from_url(model_url, progress=True, map_location=torch.device('cpu'))
    if modeltype == 'unet':
        model = UNet(n_classes=n_classes, padding=True, depth=5, up_mode='upsample', batch_norm=True, residual=False)
    elif modeltype == 'resunet':
        model = UNet(n_classes=n_classes, padding=True, depth=5, up_mode='upsample', batch_norm=True, residual=True)
    else:
        print(f"Model {modelname} not known")
    model.load_state_dict(state_dict)
    model.eval()
    if torch.cuda.is_available():
        device = torch.device('cuda')
    else:
        print("No GPU support available, will use CPU. Note, that this is significantly slower!")
        batch_size = 1
        device = torch.device('cpu')
    model.to(device)
    return model
    
    return model

_lung_mask_model=None 

def lung_model():
    global _lung_mask_model
    if _lung_mask_model is None: 
        _lung_mask_model = get_model(model_url=lungmask_modelfile)
    return _lung_mask_model


def get_lung_mask(fname, model):
    input_image = sitk.ReadImage(fname)
    segmentation = mask.apply(input_image, model=model)  # default model is U-net(R231)
    nda = sitk.GetArrayFromImage(input_image)
    nda[segmentation == 0] = 0
    nda[nda == -2000] = 0
    return segmentation


def sitk2img(img):
    nda = sitk.GetArrayFromImage(img)
    return nda


def dicom_to_image(filename):
    im = dcm.dcmread(filename)
    img = im.pixel_array
    img[img == -2000] = 0
    return img


def get_users(is_train):
    if is_train:
        folder = os.path.join(OSIC_INPUT_HOME, 'train')
    else:
        folder = os.path.join(OSIC_INPUT_HOME, 'test')
    from os import listdir
    from os.path import isdir, join
    users = [f for f in listdir(folder) if isdir(join(folder, f))]
    return users


def get_dcm_files(uid, is_train):
    if is_train:
        folder = os.path.join(OSIC_INPUT_HOME, 'train')
    else:
        folder = os.path.join(OSIC_INPUT_HOME, 'test')
    return glob.glob("{}/{}/*.dcm".format(folder, uid))


def _get_user_image_fast(files):
    if 1:
        files = sorted(files, key=lambda u: int(u.split("/")[-1].split(".")[0]))
        assert len(files) > 0
        reader = sitk.ImageSeriesReader()
        reader.SetFileNames(files)
        vol = reader.Execute()    
        return vol

    
def _get_user_image_slow(files):
    if 1:
        x = []
        shape = None
        for u in files:
            try:
                i = dicom_to_image(u)
                shape = i.shape
                x.append(i)
            except:
                i = None
                print("ignore ", u)
        
        if shape is None:
            return None
        x = [np.zeros(shape) + np.nan if u is None else u  for u in x]
            
        x = np.array(x)
        return sitk.GetImageFromArray(x)
    
    
def get_user_image(uid, is_train):
    files = get_dcm_files(uid, is_train)
    try:
        return _get_user_image_fast(files)
    except:
        return _get_user_image_slow(files)


def get_user_mask(uid, is_train):
    input_image = get_user_image(uid, is_train)
    # input_image=sitk.GetImageFromArray(input_image)
    segmentation = mask.apply(input_image, model=lung_model())  # default model is U-net(R231)
    nda = sitk.GetArrayFromImage(input_image)
    nda[segmentation == 0] = 0
    return segmentation, nda

        
def save_array_z(arr, filepath):
    with gzip.GzipFile(filepath, "w", compresslevel=0) as f:
        numpy.save(file=f, arr=arr)


def load_array_z(filepath):
    with gzip.GzipFile(filepath, "r") as f:
        return numpy.load(file=f)

        
def scan_sickness(uid, is_train):
    files = get_dcm_files(uid, is_train)
    files = sorted(files, key=lambda u: int(u.split("/")[-1].split(".")[0]))
    
    slices = [dcm.dcmread(fname, defer_size=100, stop_before_pixels=True, force=True) for fname in files]
    x = []
    for s in slices:
        if hasattr(s, 'SliceThickness'):
            x.append(float(s.SliceThickness))
            # x.append(float(s['0018', '0050'].value))
    if len(x) * 2 > len(slices):
        return np.median(x)
        
    x = []
    for i in range(1, len(slices)):
        if hasattr(slices[i - 1], 'ImagePositionPatient') and hasattr(slices[i], 'ImagePositionPatient'):
            x.append(np.abs(slices[i - 1].ImagePositionPatient[2] - slices[i].ImagePositionPatient[2]))
            # x.append(np.abs(slices[i-1]['0020', '0032'].value[2] - slices[i]['0020', '0032'].value[2]))
    if len(x) * 2 > len(slices):
        return np.median(x)        
    
    x = []
    for i in range(1, len(slices)):
        if hasattr(slices[i - 1], 'SliceLocation') and hasattr(slices[i], 'SliceLocation'):
            # x.append(np.abs(float(slices[i-1]['0020', '1041'].value) - float(slices[i]['0020', '1041'].value)))
            x.append(np.abs(slices[i - 1].SliceLocation - slices[i].SliceLocation))
    if len(x) * 2 > len(slices):
        return np.median(x)        
    
    return None


def _scan_sickness(uid, is_train):
    from joblib import Parallel, delayed
    return Parallel(n_jobs=2)(delayed(_scan_sickness)(*arg) for arg in [(uid, is_train)])[0]
