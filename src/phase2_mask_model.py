'''
Created on Aug 15, 2020

@author: bo
'''

from skimage.measure import label, regionprops, regionprops_table
from skimage.transform import  resize
import config
import os
import pickle
import json    
import gzip
import numpy as np 

import tensorflow as tf 

from tensorflow.keras import layers
from model.impala_cnn_tf import make_model
from tensorflow.keras.utils import multi_gpu_model
import sys
import utils
from phase1_mask_model import central_img, _int64_feature, _bytes_feature, \
    image_feature_description


def _load_raw_data():
    with gzip.open(os.path.join(config.OSIC_HOME, "info", "phase2_mask_postives.json.gz")) as f:
        pos = set(json.load(f))
        print("loadded {} positive labels".format(len(pos)))
            
    files = sorted(os.listdir(os.path.join(config.OSIC_SANDBOX_HOME, "train_phase2_masks")))
    masks = []
    for fname in files:
        a = utils.pickle_loadz(os.path.join(config.OSIC_SANDBOX_HOME, "train_phase2_masks", fname))
        masks += a
    len(masks)
    print("loadded {} records".format(len(masks)))

    ret = []
    for i, x in enumerate(masks):
        label = 1 if i in pos else 0 
        ret.append([label, x])
    np.random.shuffle(ret)    
    return ret


def create_input_data(input_folder, imgsize):
    options = tf.io.TFRecordOptions(compression_type='GZIP')
    data = _load_raw_data()
    batch_size = len(data) // 128
    n_files = int(np.ceil(len(data) / batch_size))
    k = 0
    for i in range(n_files):
    # for i in range(3):
        filename = os.path.join(input_folder, "part{}.tfrecord".format(i))
        print("Writing " + filename)
        with tf.io.TFRecordWriter(filename, options=options) as writer: 
            for _ in range(batch_size):
                if k < len(data):
                    label, img = data[k]
                    img = (central_img(img * 255, PHASE1_IMGSIZE_BEFORE_RESHAPE, final_size=imgsize)) / 255.0;
                    img = np.expand_dims(img, -1).astype(np.float32)
                    feature = {
                        'height': _int64_feature(img.shape[0]),
                        'width': _int64_feature(img.shape[1]),
                        'channel': _int64_feature(1),
                        'label': _int64_feature(label),
                        'img_raw': _bytes_feature(img.reshape(-1).tobytes()),
                    }    
                    example = tf.train.Example(features=tf.train.Features(feature=feature))
                    writer.write(example.SerializeToString())
                    k += 1


def _test_read_data(datafolder):
    import matplotlib.pyplot as plt
    filenames = [os.path.join(datafolder, u) for u in os.listdir(datafolder)]
    raw_dataset = tf.data.TFRecordDataset(filenames=filenames, compression_type='GZIP')

    def _parse_image_function(example_proto):
        # Parse the input tf.Example proto using the dictionary above.
        return tf.io.parse_single_example(example_proto, image_feature_description)

    parsed_image_dataset = raw_dataset.map(_parse_image_function)
    
    plt.figure(figsize=(10, 10))
    for i, image_features in enumerate(parsed_image_dataset.take(9)):
        _ax = plt.subplot(3, 3, i + 1)
        img_raw = image_features['img_raw'].numpy()
        shape = (image_features['height'].numpy().astype("int")
            , image_features['width'].numpy().astype("int"),
            image_features['channel'].numpy().astype("int"))            
        img = np.frombuffer(img_raw, dtype=np.float32).reshape(shape)
        print(img.min(), img.max())
        plt.imshow(img)
        plt.title(str(image_features['label'].numpy().astype("uint8")))
        plt.axis("off")
    plt.show()

        
def train_model_with_files(batch_size=128, imgsize=(128, 128)):
    filenames = [os.path.join(datafolder, u) for u in os.listdir(datafolder)]
    dataset = tf.data.TFRecordDataset(filenames=filenames, compression_type='GZIP')

    def _parse_image_function(example_proto):
        # Parse the input tf.Example proto using the dictionary above.
        return tf.io.parse_single_example(example_proto, image_feature_description)

    dataset = dataset.map(_parse_image_function)
    
    def _to_image_and_label(image_features):
        img_raw = tf.io.decode_raw(image_features['img_raw'], out_type=tf.float32)
        img_shape = tf.stack([image_features['height'], image_features['width'], image_features['channel']])
        img = tf.reshape(img_raw, img_shape)
        return img, image_features['label']
    
    dataset = dataset.map(_to_image_and_label)

    def is_test(x, y):
        return x % 5 == 0
    
    def is_train(x, y):
        return not is_test(x, y)
        # return True
    
    recover = lambda _x, y: y
    
    val_ds = dataset.enumerate() \
                        .filter(is_test) \
                        .map(recover).batch(batch_size)
    
    train_ds = dataset.enumerate() \
                        .filter(is_train) \
                        .map(recover).batch(batch_size).shuffle(100)   
    input_shape = (*imgsize, 1)
    
    if True:
        data_augmentation = tf.keras.Sequential([
          layers.experimental.preprocessing.RandomFlip("horizontal_and_vertical"),
          layers.experimental.preprocessing.RandomRotation(0.08, fill_mode='constant'),
          layers.experimental.preprocessing.RandomZoom((-0.2, 0.2), fill_mode='constant'),
          layers.experimental.preprocessing.RandomTranslation(height_factor=(-0.2, 0.2), width_factor=(-0.1, 0.1), fill_mode='constant'),
        ])
        train_ds = train_ds.map(lambda x, y: (data_augmentation(x, training=True), y))
    
    if False:
        import matplotlib.pyplot as plt
        plt.figure(figsize=(10, 10))
        for images, labels in train_ds.take(1):
            for i in range(9):
                _ax = plt.subplot(3, 3, i + 1)
                print(images[i].numpy().min(), images[i].numpy().max())
                plt.imshow((images[i].numpy() * 255).astype("uint8"))
                plt.title(str(labels[i].numpy().astype("uint8")))
                plt.axis("off")
        plt.show()    
    _train_model(input_shape, train_ds, val_ds, None, None)


def _train_model(input_shape, train_ds, val_ds, steps_per_epoch, validation_steps):
    model = make_model(input_shape=input_shape)
    # (model.summary())

    try:
        model = multi_gpu_model(model, cpu_merge=False)
        print("Training using multiple GPUs..")
    except:
        print("Training using single GPU or CPU..")
         
    model.compile(optimizer='adam',
              loss=tf.keras.losses.SparseCategoricalCrossentropy(from_logits=True),
              metrics=['accuracy'])

    AUTOTUNE = tf.data.experimental.AUTOTUNE
    train_ds = train_ds.prefetch(buffer_size=AUTOTUNE)
    val_ds = val_ds.prefetch(buffer_size=AUTOTUNE)

    tensorboard_callback = tf.keras.callbacks.TensorBoard(log_dir="./logs")
    earlystopping_callback = tf.keras.callbacks.EarlyStopping(monitor='val_loss', patience=5)
    model_checkpoint_callback = tf.keras.callbacks.ModelCheckpoint(
        filepath="./phase2_mask/phase2_mask",
        save_weights_only=True,
        monitor='val_loss',
        mode='max',
        save_best_only=True)

    epochs = 50
    history = model.fit(
      train_ds,
      validation_data=val_ds,
      epochs=epochs,
      steps_per_epoch=steps_per_epoch,
      validation_steps=validation_steps,
      callbacks=[tensorboard_callback, earlystopping_callback, model_checkpoint_callback]
      
    )
    
    if False:
        import matplotlib.pyplot as plt
        print(list(history.history.keys()))
        acc = history.history['accuracy']
        val_acc = history.history['val_accuracy']
        
        loss = history.history['loss']
        val_loss = history.history['val_loss']
        
        epochs_range = range(epochs)
        
        plt.figure(figsize=(8, 8))
        plt.subplot(1, 2, 1)
        plt.plot(epochs_range, acc, label='Training Accuracy')
        plt.plot(epochs_range, val_acc, label='Validation Accuracy')
        plt.legend(loc='lower right')
        plt.title('Training and Validation Accuracy')
        
        plt.subplot(1, 2, 2)
        plt.plot(epochs_range, loss, label='Training Loss')
        plt.plot(epochs_range, val_loss, label='Validation Loss')
        plt.legend(loc='upper right')
        plt.title('Training and Validation Loss')
        plt.show()


PHASE1_IMGSIZE = (128, 128)        
PHASE1_IMGSIZE_BEFORE_RESHAPE = (256 * 2, 256 * 2)
    
if __name__ == '__main__':
    batch_size = 128
    imgsize = PHASE1_IMGSIZE

    datafolder = os.path.join(config.OSIC_INPUT_HOME, "phase2_mask_train")    
    if len(sys.argv) == 2 and sys.argv[1] == 'create_data':
        config.create_folder_if_not_exists(datafolder)
        print("creating datafiles in " + datafolder)
        create_input_data(datafolder, imgsize=imgsize)
    elif len(sys.argv) == 2 and sys.argv[1] == 'test_data':
        _test_read_data(datafolder)        
    elif len(sys.argv) == 3 and sys.argv[1] == 'train':
        if sys.argv[2] == 'file':
            print ("training with data in file system.")
            if not os.path.exists(datafolder):
                print("data not found, use create_data first")
            else:
                train_model_with_files(batch_size=batch_size, imgsize=imgsize)
    else:
        print("""
        usage: 
            python {} train <memory|file>
            or
            python {} create_data
        """.format(sys.argv[0], sys.argv[0]))
