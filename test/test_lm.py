'''
Created on Aug 13, 2020

@author: bo
'''
import unittest

import lm_andrada as lm 
class Test(unittest.TestCase):


    def setUp(self):
        pass


    def tearDown(self):
        pass


    def test_get_users(self):
        users = lm.get_users(is_train=False)
        self.assertEqual(5, len(users))
        self.assertTrue('ID00423637202312137826377' in users)
        users = lm.get_users(is_train=True)
        self.assertEqual(176, len(users))
        self.assertTrue('ID00423637202312137826377' in users)

    
    def test_get_user_masks(self):
        user = 'ID00419637202311204720264'
        masks,images = lm.get_user_mask(user,  False)
        self.assertEqual(28,len(masks))
    
if __name__ == "__main__":
    #import sys;sys.argv = ['', 'Test.test_get_users']
    unittest.main()